cmake_minimum_required(VERSION 3.17)
project(jsondemo)

set(CMAKE_CXX_STANDARD 11)

add_executable(jsondemo main.cpp)