#ifndef JSONDEMO_TESTSTRUCT_H
#define JSONDEMO_TESTSTRUCT_H
#include "json.hpp"
#include <string>
using json = nlohmann::json;

// To serialize some specific struct from and to file
namespace loader {
    struct Test {
        int id;
        std::string test;
    };
    NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(Test, id, test)
}

using namespace  loader;
#endif //JSONDEMO_TESTSTRUCT_H
