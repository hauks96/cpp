#include "cereal/archives/binary.hpp"
#include <fstream>
#include <string>
#include "Book.h"

int main()
{
    Book some_book(1014, "Harry Potter", "Megan Fox");

    std::ofstream os("books.cereal", std::ios::binary);
    cereal::BinaryOutputArchive save_archive( os );

    save_archive(some_book);

    std::ifstream is("books.cereal", std::ios::binary);
    cereal::BinaryInputArchive load_archive(is);

    Book loaded_book;
    load_archive(loaded_book);

    return 0;
}
