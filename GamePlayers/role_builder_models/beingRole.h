#ifndef HW4_BEINGROLE_H
#define HW4_BEINGROLE_H
#include "../json.hpp"
using json = nlohmann::json;

namespace role_being{
    struct loader {
        std::string Type;
        int MaxLife;
        int MinLife;
        int MaxStrength;
        int MinStrength;
        int MaxIntelligence;
        int MinIntelligence;
    };
    NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(loader, Type, MaxLife, MinLife, MaxStrength, MinStrength, MaxIntelligence, MinIntelligence)
}
#endif //HW4_BEINGROLE_H
