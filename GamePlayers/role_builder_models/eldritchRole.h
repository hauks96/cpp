#ifndef HW4_ELDRITCHROLE_H
#define HW4_ELDRITCHROLE_H
#include "../json.hpp"
using json = nlohmann::json;


namespace role_eldritch {
    struct loader {
        int Traumatism;
    };
    NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(loader, Traumatism)
}


#endif //HW4_ELDRITCHROLE_H
