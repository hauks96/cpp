#ifndef HW4_ELDRITCHCHARACTER_H
#define HW4_ELDRITCHCHARACTER_H
#include "../json.hpp"
using json = nlohmann::json;


namespace char_eldritch {
    struct loader {
        int Traumatism;
    };
    NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(loader, Traumatism)
}

#endif //HW4_ELDRITCHCHARACTER_H
