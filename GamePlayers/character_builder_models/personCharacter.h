#ifndef HW4_PERSONCHARACTER_H
#define HW4_PERSONCHARACTER_H
#include "../json.hpp"
using json = nlohmann::json;


namespace char_person {
    struct loader {
        string Name;
        string Role;
        string Gender;
        int Fear;
        int Terror;
    };
    NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(loader, Name, Role, Gender, Fear, Terror)
}
#endif //HW4_PERSONCHARACTER_H
