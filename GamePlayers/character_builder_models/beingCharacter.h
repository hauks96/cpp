#ifndef HW4_BEINGCHARACTER_H
#define HW4_BEINGCHARACTER_H
#include "../json.hpp"
using json = nlohmann::json;


namespace char_being {
    struct loader {
        std::string Type;
        int Life;
        int Strength;
        int Intelligence;
    };
    NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(loader, Type, Life, Strength, Intelligence)
}

#endif //HW4_BEINGCHARACTER_H
