#ifndef HW4_STRINGATTRIBUTE_H
#define HW4_STRINGATTRIBUTE_H
#include "Attribute.h"
#include "../json.hpp"
using json = nlohmann::json;


class StringAttribute: public Attribute {
public:
    StringAttribute(const string& name, const string& value): value(const_cast<string &>(value)), Attribute(name){
        this->value = value;
    }
    void printAttribute() override{
        Attribute::printAttributeName();
        cout << value << endl;
    }
    json value_to_json() override{
        json j_string = value;
        return j_string;
    }

    string& value;
};



#endif //HW4_STRINGATTRIBUTE_H
