#ifndef GAMEPLAYERS_INTERFACE_H
#define GAMEPLAYERS_INTERFACE_H
#include "../loaders/Roles.h"
#include "../loaders/Characters.h"
#include "../categories/Creature.h"
#include "../categories/Person.h"
#include "../categories/EldritchHorror.h"
#include "../categories/Being.h"
#include "../role_builder_models/creatureRole.h"
#include "../role_builder_models/beingRole.h"
#include "../role_builder_models/personRole.h"
#include "../role_builder_models/eldritchRole.h"


class Interface {
public:
    Interface();
    void run();

protected:
    void mainMenu();
    void characterMenu();
    void find();
    void save();
    void load();
    void print();

    void editCharacter();
    void deleteCharacter();
    void createCharacter();
    void printCharacter();

    void createRole();
    void deleteRole();

    static void invalidNumber();
    static void mainMenuCommand();
    static int getNumberInput();
    static std::string getStringInput();

    void changeName();
    void changeLife();
    void changeStrength();
    void changeIntelligence();

    void changePersonFear(bool isInvestigator);
    void changePersonGender(bool isInvestigator);
    void makePersonInvestigator();
    void changeInvestigatorTerror();
    void changeCreatureNaturalness();
    void changeCreatureDisquiet();
    void changeEldritchTraumatism();

    void createPersonCharacter(bool isInvestigator);
    void createCreatureCharacter();
    void createEldritchCharacter();

    std::string getRoleFromUser(const std::string& type);

    static role_being::loader* getBeingFromUser(const std::string& type);
    static role_person::loader* getPersonFromUser();
    static role_eldritch::loader* getEldritchFromUser();
    static role_creature::loader* getCreatureFromUser();


private:
    bool characterSelected = false;
    enum character_types { NONE, PERS, INV, CRE, ELD };
    character_types selected_type = NONE;

    // If selected
    Person* selected_person = nullptr;
    Person* selected_investigator = nullptr;
    Creature* selected_creature = nullptr;
    EldritchHorror* selected_eldritch = nullptr;
    Being* selected_being = nullptr;


    bool wantsQuit = false;
    bool wantsMainMenu = false;

    Roles* roles;
    Characters* characters;
};


#endif //GAMEPLAYERS_INTERFACE_H
