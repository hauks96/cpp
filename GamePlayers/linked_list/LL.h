#ifndef HW4_LL_H
#define HW4_LL_H
#include "node.h"
#include <iostream>
/*
 * Implemented mainly to have something with template implementation
 */

template<class T>
class LL {
public:
    /*
      Push front O(1)
      Push back  O(1)
      Pop front  O(1)
      Pop back   O(n)
    */
    LL(){
        head = nullptr;
        tail = nullptr;
        size = 0;
    }
    ~LL(){
        // Just makes life easier not to make a delete constructor in node
        while(head!= nullptr){
            Node<T>* temp_head = head;
            head = head->next;
            delete temp_head;
        }
    }

    void push_back(T value){
        if (head == nullptr){
            head = new Node<T>(value);
            tail = head;
        }
        else {
            tail->next = new Node<T>(value);
            tail = tail->next;
        }
        size ++;
    }

    void push_front(T value){
        if (head == nullptr){
            head = new Node<T>(value);
            tail = head;
        }
        else {
            auto* new_head = new Node<T>(value);
            new_head->next = head;
            head = new_head;
        }
        size++;
    }

    void pop_front(){
        if (head == nullptr){
            return;
        }
        if (head == tail){
            delete head;
            head = nullptr;
            tail = nullptr;
            size--;
        }
        else {
            Node<T> *temp_head = head;
            head = head->next;
            delete temp_head;
            size--;
        }
    }

    void pop_back(){
        // O(n) because I'm too lazy for dll
        if (head == nullptr){
            return;
        }
        if (head == tail){
            delete head;
            head = nullptr;
            tail = nullptr;
            size--;
        }
        else {
            Node<T> *temp_next = head;
            while(temp_next->next != tail){
                temp_next = temp_next->next;
            }
            Node<T> *temp_tail = tail;
            tail = temp_next;
            tail->next = nullptr;
            delete temp_tail;
            size--;
        }
    }
    T& getNode(int index){
        /* Returns a reference to the node data */
        if (index<0 || index>size){
            throw std::runtime_error("Out of bounds exception in getNode(index): LL - line: 100");
        }
        int start = 0;
        Node<T>* temp_next = head;
        while(start!=index){
            temp_next = temp_next->next;
            start++;
        }
        return temp_next->data;
    }

    int getSize() const{
        return size;
    }

private:
    Node<T>* head;
    Node<T>* tail;
    int size;
};


#endif //HW4_LL_H
