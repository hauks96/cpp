#ifndef HW4_BEING_H
#define HW4_BEING_H
#include "../linked_list/LL.h"
#include "../attributes/IntAttribute.h"
#include "../attributes/StringAttribute.h"
#include "../attributes/BoolAttribute.h"
#include "../json.hpp"
#include "../role_builder_models/beingRole.h"
#include "../character_builder_models/beingCharacter.h"
using namespace std;
/*
 * This implementation including all subclasses is the literal definition of overkill
 * and mostly done because of implementation requirements.
 *
 * Most of the assignment could be defined with some structs and a json methods.
 *
 *
 * life
 * strength
 * intelligence
 */

class Being {
public:
    // Role constructor
    explicit Being(role_being::loader& base_loader);
    // Character constructor
    explicit Being(char_being::loader& base_loader);

    void changeLife(int newLife);

    void changeStrength(int newStrength);

    void changeIntelligence(int newIntelligence);

    void printMaxMinLife() const;
    void printMaxMinStrength() const;
    void printMaxMinIntelligence() const;

    virtual void changeName(string newName);

    // This method allows us to print the attributes from the parent class
    void printAttributes();
    // Generate a random number in a given range
    static int random_number(int from, int to);
    // Generate an invalid message for something
    static void invalid_message(const string& what);

    const string& getRole();
    const string& getType();
    const string& getName();
    char_being::loader* getBeingData();

    // Converts any subclass to json format
    virtual void to_json(json& j);
    role_being::loader* getRoleSettings();
    void loadRoleSettings(role_being::loader& being_settings);
    void loadCharacterSettings(role_being::loader& being_settings);

protected:
    bool isRole;
    int life = 0;
    int strength = 0;
    int intelligence = 0;
    string type = "Being";
    string role = "none";
    string name = "none";
    LL<Attribute*> attributes; // Contains all attributes of any subclass

    void addAttribute(const string& attributeName, const string& value);
    void addAttribute(const string& attributeName, int& value);
    void addAttribute(const string& attributeName, bool& value);
    static void errorModifyRoles();

private:
    int maxLife = 10;
    int minLife = 0;
    int maxStrength = 10;
    int minStrength = 0;
    int maxIntelligence = 10;
    int minIntelligence = 0;
};


#endif //HW4_BEING_H
