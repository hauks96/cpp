#ifndef HW4_CREATURE_H
#define HW4_CREATURE_H
#include "Being.h"
#include "../json.hpp"
#include "../role_builder_models/beingRole.h"
#include "../role_builder_models/creatureRole.h"
#include "../character_builder_models/beingCharacter.h"
#include "../character_builder_models/creatureCharacter.h"
using namespace std;
/*
 * life
 * strength
 * intelligence
 * type
 * ----
 * disquiet
 * unnatural
 * role
 * name
 */
class Creature: public Being {
public:
    // Constructor for role loading
    Creature(role_being::loader &being_rl, role_creature::loader &role_loader);

    // Constructor for character loading
    Creature(char_being::loader &being_cl, char_creature::loader &creature_loader);

    void changeName(string newName) override;
    // Use instead of changeName when adding default name
    void changeNPCname(string newName);
    bool named() const;

    void to_json(json& j) override;
    virtual void changeNaturalness();
    virtual void changeDisquiet(int newDisquiet);

    char_creature::loader* getCreatureData();

protected:
    bool unnatural;
    int disquiet;

private:
    bool hasName = false;
    int maxDisquiet = 10;
};

#endif //HW4_CREATURE_H
