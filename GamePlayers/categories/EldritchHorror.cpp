#include "EldritchHorror.h"

EldritchHorror::EldritchHorror(role_being::loader &being_rl, role_creature::loader &creature_rl,
                               role_eldritch::loader &role_loader):Creature(being_rl, creature_rl) {
    if (role_loader.Traumatism>maxTraumatism) traumatism = maxTraumatism;
    else if (role_loader.Traumatism<0) traumatism = minTraumatism;
    else traumatism = role_loader.Traumatism;
    addAttribute("Traumatism", traumatism);
}


EldritchHorror::EldritchHorror(char_being::loader &being_cl, char_creature::loader &creature_cl,
                               char_eldritch::loader &char_loader):Creature(being_cl, creature_cl) {
    if (char_loader.Traumatism>maxTraumatism) traumatism = maxTraumatism;
    else if (char_loader.Traumatism<0) traumatism = minTraumatism;
    else traumatism = char_loader.Traumatism;
    addAttribute("Traumatism", traumatism);
}

void EldritchHorror::changeNaturalness() {
    cout << "The horror will remain unnatural!" << endl;
}

void EldritchHorror::changeDisquiet(int newDisquiet){
    cout << "You can hear the screams all around you... AN ELDRITCH WILL NOT BE SILENCED!" << endl;
}

void EldritchHorror::changeTraumatism(int newTraumatism) {
    if (newTraumatism<=maxTraumatism && newTraumatism<minTraumatism){
        traumatism = newTraumatism;
        return;
    }
    invalid_message("traumatism");
}

char_eldritch::loader *EldritchHorror::getEldritchData() {
    auto* eldritch_data = new char_eldritch::loader;
    eldritch_data->Traumatism = traumatism;
    return eldritch_data;
}
