#ifndef HW4_ELDRITCHHORROR_H
#define HW4_ELDRITCHHORROR_H
#include "Creature.h"
#include "../role_builder_models/beingRole.h"
#include "../role_builder_models/creatureRole.h"
#include "../role_builder_models/eldritchRole.h"
#include "../character_builder_models/beingCharacter.h"
#include "../character_builder_models/creatureCharacter.h"
#include "../character_builder_models/eldritchCharacter.h"
using namespace std;
/*
 * life
 * strength
 * intelligence
 * type
 * traumatism
 * disquiet
 * unnatural
 * ----
 * role
 * name
 */

class EldritchHorror: public Creature {
public:
    // Constructor for role loading
    EldritchHorror(role_being::loader& being_rl, role_creature::loader& creature_rl, role_eldritch::loader& role_loader);
    // Constructor for character loading
    EldritchHorror(char_being::loader& being_cl, char_creature::loader& creature_cl, char_eldritch::loader& char_loader);

    void changeNaturalness() override;
    void changeDisquiet(int newDisquiet) override;
    void changeTraumatism(int newTraumatism);
    char_eldritch::loader* getEldritchData();

protected:
    int traumatism = 0;
private:
    int maxTraumatism = 3;
    int minTraumatism = 0;
};

#endif //HW4_ELDRITCHHORROR_H
