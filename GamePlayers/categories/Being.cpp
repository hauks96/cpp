#include "Being.h"
#include <iostream>
using namespace std;

Being::Being(role_being::loader &base_loader) {
    isRole = true;
    type = base_loader.Type;
    minLife = base_loader.MinLife;
    maxLife = base_loader.MaxLife;
    minStrength = base_loader.MinStrength;
    maxStrength = base_loader.MaxStrength;
    minIntelligence = base_loader.MinIntelligence;
    maxIntelligence = base_loader.MaxIntelligence;
    addAttribute("Type", type);
    addAttribute("MaxLife", maxLife);
    addAttribute("MinLife", minLife);
    addAttribute("MaxStrength", maxStrength);
    addAttribute("MinStrength", minStrength);
    addAttribute("MaxIntelligence", maxIntelligence);
    addAttribute("MinIntelligence", minIntelligence);
}

Being::Being(char_being::loader &base_loader) {
    isRole = false;
    type = base_loader.Type;
    life = base_loader.Life;
    strength = base_loader.Strength;
    intelligence = base_loader.Intelligence;
    addAttribute("Type", type);
    addAttribute("Life", life);
    addAttribute("Strength", strength);
    addAttribute("Intelligence", intelligence);
}

int Being::random_number(int from, int to) {
    if (from == to) return from;
    return from + (rand() % (1+(to-from)));
}

void Being::invalid_message(const string &what) {
    cout << "Invalid " << what << " entered" << endl;
}

void Being::changeLife(int newLife) {
    if (isRole){
        errorModifyRoles();
        return;
    }
    if (newLife<=maxLife && newLife>=minLife){
        life = newLife;
        return;
    }
    invalid_message("life");
}

void Being::changeStrength(int newStrength) {
    if (isRole){
        errorModifyRoles();
        return;
    }
    if (newStrength<=maxStrength && newStrength>=minStrength){
        strength = newStrength;
        return;
    }
    invalid_message("strength");
}

void Being::changeIntelligence(int newIntelligence) {
    if (isRole){
        errorModifyRoles();
        return;
    }
    if (newIntelligence<= maxIntelligence && newIntelligence>=minIntelligence){
        intelligence = newIntelligence;
        return;
    }
    invalid_message("intelligence");
}

void Being::printAttributes() {
    cout << endl;
    int size = attributes.getSize();
    for (int i=0; i<size; i++){
        attributes.getNode(i)->printAttribute();
    }
}

const string &Being::getRole() {
    return role;
}

const string &Being::getType() {
    return type;
}

char_being::loader *Being::getBeingData() {
    auto* being_data = new char_being::loader;
    being_data->Type = type;
    being_data->Intelligence = intelligence;
    being_data->Strength = strength;
    being_data->Life = life;
    return being_data;
}

void Being::changeName(string newName) {
    if (isRole){
        errorModifyRoles();
    }
    this->name = move(newName);
}

void Being::to_json(json &j) {
    int size = attributes.getSize();
    for (int i=0; i<size; i++){
        Attribute* attr = attributes.getNode(i);
        string attr_name = attr->name;
        json j_value = attr->value_to_json();
        j[attr_name] = j_value;
    }
}

void Being::addAttribute(const string &attributeName, const string &value) {
    Attribute* new_attr = new StringAttribute(attributeName, value);
    if (attributeName == "Name" || attributeName == "Role" || attributeName=="Type") attributes.push_front(new_attr);
    else attributes.push_back(new_attr);
}

void Being::addAttribute(const string &attributeName, int &value) {
    Attribute* new_attr = new IntAttribute(attributeName, value);
    attributes.push_back(new_attr);
}

void Being::addAttribute(const string &attributeName, bool &value) {
    Attribute* new_attr = new BoolAttribute(attributeName, value);
    attributes.push_back(new_attr);
}

void Being::errorModifyRoles(){
    cout << "Cannot change role attributes." << endl;
}

const string &Being::getName() {
    return name;
}

role_being::loader *Being::getRoleSettings() {
    if (!isRole){
        throw std::runtime_error("Trying to get settings from a character constructed being");
    }
    auto* being_settings = new role_being::loader;
    being_settings->Type = type;
    being_settings->MaxLife = maxLife;
    being_settings->MinLife= minLife;
    being_settings->MaxStrength = maxStrength;
    being_settings->MinStrength = minStrength;
    being_settings->MaxIntelligence = maxIntelligence;
    being_settings->MinIntelligence = minIntelligence;
    return being_settings;
}

void Being::loadRoleSettings(role_being::loader& being_settings) {
    minLife = being_settings.MinLife;
    maxLife = being_settings.MaxLife;
    minStrength = being_settings.MinStrength;
    maxStrength = being_settings.MaxStrength;
    minIntelligence = being_settings.MinIntelligence;
    maxIntelligence = being_settings.MaxIntelligence;
    life = random_number(minLife, maxLife);
    strength = random_number(minStrength, maxStrength);
    intelligence = random_number(minIntelligence, maxIntelligence);
}

void Being::loadCharacterSettings(role_being::loader &being_settings) {
    minLife = being_settings.MinLife;
    maxLife = being_settings.MaxLife;
    minStrength = being_settings.MinStrength;
    maxStrength = being_settings.MaxStrength;
    minIntelligence = being_settings.MinIntelligence;
    maxIntelligence = being_settings.MaxIntelligence;
}

void Being::printMaxMinLife() const {
    std::cout << "Max: " << maxLife << ", Min: " << minLife << std::endl;
}

void Being::printMaxMinStrength() const {
    std::cout << "Max: " << maxStrength << ", Min: " << minStrength << std::endl;
}

void Being::printMaxMinIntelligence() const {
    std::cout << "Max: " << maxIntelligence << ", Min: " << minIntelligence << std::endl;
}


