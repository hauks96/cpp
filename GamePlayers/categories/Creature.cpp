#include "Creature.h"

Creature::Creature(role_being::loader &being_rl, role_creature::loader &role_loader):Being(being_rl) {
    role = role_loader.Role;
    unnatural = role_loader.Unnatural;

    if (role_loader.Disquiet > maxDisquiet) disquiet = maxDisquiet;
    else if (role_loader.Disquiet<0) disquiet = 0;
    else disquiet = role_loader.Disquiet;

    addAttribute("Role", role);
    addAttribute("Disquiet", disquiet);
    addAttribute("Unnatural", unnatural);
}

Creature::Creature(char_being::loader &being_cl, char_creature::loader &creature_loader):Being(being_cl) {
    unnatural = creature_loader.Unnatural;
    disquiet = creature_loader.Disquiet;
    role = creature_loader.Role;
    name = creature_loader.Name;
    addAttribute("Role", role);
    addAttribute("Name", name);
    addAttribute("Disquiet", disquiet);
    addAttribute("Unnatural", unnatural);
    if (name == "none") hasName = false;
    else hasName = true;
}

void Creature::changeName(string newName) {
    Being::changeName(newName);
    hasName = true;
}

void Creature::changeNPCname(string newName) {
    name = move(newName);
}

bool Creature::named() const {
    return hasName;
}

void Creature::to_json(json &j) {
    if (!hasName){
        name = "none"; // Reset the name for NPCs so that we get correct enumeration when loading again
    }
    Being::to_json(j);
}

void Creature::changeDisquiet(int newDisquiet) {
    if (newDisquiet<=maxDisquiet && newDisquiet>=0){
        disquiet = newDisquiet;
        return;
    }
    invalid_message("disquiet");
}

char_creature::loader *Creature::getCreatureData() {
    auto* creature_data = new char_creature::loader;
    creature_data->Name = name;
    creature_data->Role = role;
    creature_data->Disquiet = disquiet;
    creature_data->Unnatural = unnatural;
    return creature_data;
}

void Creature::changeNaturalness() {
    unnatural = !unnatural;
}
