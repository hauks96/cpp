#ifndef PIZZAN_PIZZABAKER_H
#define PIZZAN_PIZZABAKER_H
#include <map>
#include <string>
#include "../Serializers/PizzaOrder.h"
#include "BakerOrder.h"

class PizzaBaker {
public:
    PizzaBaker() = default;
    // id (int assigned by class), BakerOrder*
    std::map<int, BakerOrder*> orders = {};
    /* Automatically removes orders that have decayed when new ones arrive */
    void add_order(PizzaOrder* pizza_order);
    /* Deallocates the memory */
    void remove_order(int order_id);
    /* Automatically saves completed orders*/
    bool update_order_status(int order_id);

    bool exists(int id) const;

    BakerOrder* get_order(int order_id);
    PizzaStates get_order_state(int order_id);

private:
    int next_id = 0;
};

#endif //PIZZAN_PIZZABAKER_H
