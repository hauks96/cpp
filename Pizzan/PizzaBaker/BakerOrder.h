#ifndef PIZZAN_BAKERORDER_H
#define PIZZAN_BAKERORDER_H
#include "../Serializers/PizzaOrder.h"
#include "PizzaStates.h"
#include "../date.h"

class BakerOrder {
public:
    BakerOrder(PizzaOrder* order, PizzaStates state):
        order(order), state(state){
        time = std::chrono::system_clock::now();
        auto minute_precision_time = date::floor<std::chrono::minutes>(time);
        time_string = date::format("%Y-%m-%d %H:%M", minute_precision_time);
        update_status(state);
    }

    ~BakerOrder()
    {
        for (auto pizza: order->pizzas)
            delete pizza;
    }

    void update_status(PizzaStates new_status){
        state = new_status;
        switch(new_status)
        {
            case RECEIVED:
                status_string = "RECEIVED";
                break;
            case PREPARATION:
                status_string = "PREPARATION";
                break;
            case IN_OVEN:
                status_string = "IN OVEN";
                break;
            case COMPLETE:
                status_string = "COMPLETED";
                break;
            default:
                status_string = "RECEIVED";
                break;
        }
    }

    std::chrono::time_point<std::chrono::system_clock> time;
    std::string time_string;
    std::string status_string;
    PizzaOrder* order;
    int state;

    PizzaStates getState() const
    {
        return PizzaStates(state);
    }
};
#endif //PIZZAN_BAKERORDER_H
