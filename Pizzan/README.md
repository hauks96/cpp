# Pizzan software
_Author: Ægir Máni Hauksson, aegir19@ru.is_ \
This software is made up of 4 parts which are the following.
1. A customer terminal program to order pizzas
2. A bakery outlet terminal to manage incoming pizza orders
3. A delivery outlet terminal to manage incoming pizza deliveries
4. A main server which communicates and forwards request between the 3 mentioned above.

I will leave the in depth details of the programs to the video as it would take me hours to write up everything 
that the programs do and how they are implemented.

Here is a simplified demonstration showing the communication process between the clients and server.
![communication](images/communication.png)

## Dependencies & limitations
To run and compile this software **you need a Linux/Mac operating system** that supports pthreads. 
I myself am on a windows machine but I encountered a tremendous headache trying to compile using threads and mutexes 
along with several other issues so I switched over to my **windows built in linux subsystem** to develop this project.

So if you want to test and you're on windows, then you can easily use the subsystem! Also I really hope you actually
test it yourself because it's really cool and I spent a lot of hours making it!

**Note:**
I have not tested compiling it on a mac OS but I assume it will work without problems.

## Compile
To compile the program you can use the supplemented [**Makefile**](Makefile).
To run it type `make` in terminal while in the root directory. 

## Run program
To test out this software you will need to use at least 4 terminal windows. The setup process goes as follows.

_All commands are ran from the project root._
1. Run the server with `./server`
2. Run the bakery program with `./bakery`
3. Run the delivery program with `./delivery`
4. Run the customer program with `./customer`

Now you can start testing by creating orders in the **customer program terminal**.

**Note**:
As long as the server is started before the clients the order in which the clients are started do not matter.
If a client is started before the server, they will not be connected for communication.

## External libraries
This project uses two external dependencies.
1. The [date.h](https://github.com/HowardHinnant/date) library to create date strings.
2. The [json.hpp](https://github.com/nlohmann/json) library to create json objects.

## Points delivered upon
1. Extensive thread usage in server and client.
2. Extensive thread management using mutex locks. See for example [Server.cpp](Networking/Server/Server.cpp)
3. De-allocation of all known objects applied to heap memory
4. Interfaces for 4 different programs, 3 of which are user interactable.
5. Extensive networking even using self made request/response headers and custom parsing.
6. Insane amount of work in one week? :P

I believe this project deserves every inch of the 100 points possible.

## Interesting solutions
1. Function pointers in listening threads to trigger external class instance methods. All the client interfaces
   implement one method which is triggered by a different thread that is listening for incoming server responses.\
   _Note that they are not static and are a part of the class instance so you can actually modify the class variables this way without ever needing access to that particular class directly._\
   For a short demonstration on how this works you can check out the file [class_member_function_invoker_demo](old_code/class_member_function_invoker_demo.cpp) and run the [test_invoker](old_code/test_invoker) executable.
   
2. Survivability measures for long term usage using thread cleanup on server.\
   The server needed a lot of linking while also ensuring that shared variables are not interchangeably accessed by different threads.
   There is no real way for the server to know which threads that it created in it's endless main loop 
   have finished running so a combination of mutex locks and a reference vector are used to join the thread
   in a different place once it's done running where it then inherently gets deleted.
   
3. There is a lot of interesting solutions buried within this mountain of code. Note though I didn't have time to comment the code much. Although I did my best in the server code.

## Shortcomings
Here are a few points I am aware of that could be improved upon given the current state of the system implementation.
1. There is no real json parsing (message receiving) error handling on the server, making it very easy to crash if the incoming data is on the incorrect format.
2. The code does not have proper commenting and is at times a bit messy as there was a lot to do in a very short time
3. Testing. There is absolutely no testing implemented and no automated tests are runnable. Furthermore I did not spend much time testing the programs.
   This is not optimal to say the least as the software is made up of 4 different executable programs which are all interlinked through server communication.
4. I did not find time to implement interfaces with an external library. Even just creating these interfaces in terminal was already a big enough job.

## Random
Here's an image depicting the states available inside the customer ordering terminal.

![customer_terminal](images/customer_interface_states.png)
