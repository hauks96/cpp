#include "Parser.h"
#include "../../json.hpp"
#include <iostream>
using json = nlohmann::json;

std::string Parser::parse_message(Response *response) {
    std::string str_message =  response->data["message"].get<std::string>();
    return str_message;
}

PizzaOrder* Parser::parse_order(Response *response) {
    PizzaOrder* pizza_order = PizzaOrder::from_json(response->data);
    return pizza_order;
}
