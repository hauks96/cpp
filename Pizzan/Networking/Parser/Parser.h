#ifndef PIZZAN_PARSER_H
#define PIZZAN_PARSER_H
#include <string>
#include "../Response/Response.h"
#include "../../Serializers/PizzaOrder.h"
class Parser {
public:
    Parser() = default;
    static std::string parse_message(Response* response);
    static PizzaOrder* parse_order(Response* response);
};


#endif //PIZZAN_PARSER_H
