#include "Server.h"
#include <iostream>
#include <mutex>
#include "../../Serializers/PizzaOrder.h"
#include "../../FileWriter/FileWriter.h"
#include "../Response/Response.h"
#include "../Request/Request.h"
#define SERVER_NAME "Server"
#define SERVER_BUFFER_LIMIT 40000
#define CLEAN_UP_INTERVAL 20

using nlohmann::json;
// TODO: Error handle json parsing - the server has basically 0 error handling and is very crash prone


/* Get the order count from the order counter file and generate an order ID from it */
std::string generate_order_id(FileWriter &fr){
    int order_number     = fr.get_next_order_id();
    std::string order_id = "PIZZAN-"+std::to_string(order_number);
    return order_id;
}

/* Parse an order within a request into a PizzaOrder object */
PizzaOrder *parse_order(Request& request){
    PizzaOrder* pizza_order = PizzaOrder::from_json(request.data);
    return pizza_order;
}

/* Parse a request message into a string */
std::string parse_message(Request& request){
    return request.data["message"].get<std::string>();
}

// Get the order ID from the request (the client socket of the customer is identified by the order id)
std::string parse_customer_target(Request& request){
    return request.data["orderID"].get<std::string>();
}

/* Get the asserted type of a client socket from the request header */
int parse_handshake(Request& request){
    int type_value = request.data["handshake"].get<int>();
    return type_value;
}

/* Finds and returns the client associated with a certain order id. If none, returns null-pointer */
ClientSocket* find_order_associated_socket(const std::string& order_id, ClientManager &clients)
{
    clients.lock_client();
    for (auto client: clients.clients)
    {
        if (!client.second->order_id.empty())
        {
            for (const auto& o_id: client.second->order_id)
            {
                if (o_id == order_id)
                {
                    clients.unlock_client();
                    return client.second;
                }
            }
        }
    }
    clients.unlock_client();
    return nullptr;
}


/* Creates the order ID for a request and returns the updated object as a new request.
 * (The request is read only that's why it's a new request)
 * */
Request* update_order_id(Request* request, FileWriter &fw, std::string &order_id){
    if (request->getType() != REQ_ORDER)
    {
        std::cout << "Update order received non-order request data" << std::endl;
        return nullptr;
    }

    PizzaOrder* pizza_order  = parse_order(*request);  // Get pizza order as object from request
    pizza_order->orderID     = generate_order_id(fw);  // Set the order id in the object
    order_id                 = pizza_order->orderID;       // change the reference value of the given order_id
    auto j_pizza_order = new json();
    PizzaOrder::to_json(*pizza_order, *j_pizza_order);     // Parse the object to json
    // Create new Request object
    auto* new_request = new Request(request->getType(), request->header.sender, *j_pizza_order, j_pizza_order);
    return new_request;                                    // Return a new request
}

/* Sends a response to a target client. This method is lock guarded by the given clients send mutex */
void socket_send(Response& response, ClientSocket& targetClient){
    targetClient.sender_mutex.lock();
    std::string response_dump = response.dump();
    send(targetClient.socket, response_dump.c_str(), response_dump.size(), 0);
    targetClient.sender_mutex.unlock();
}

/* Send a message not associated with any current request to a target client. This method will
 * automatically generate a new response with the data type RES_MESSAGE and send it to the client. */
void send_new_message(const std::string& message, ResponseStatus status_code, ClientSocket &targetClient){
    auto j_msg = new json{{"message", message}};
    Response response = Response(status_code, RES_MESSAGE, SERVER_NAME, *j_msg, j_msg);
    socket_send(response, targetClient);
}

/* Send a pre-constructed request of type REQ_MESSAGE to a target client. This will transform the request
 * into a response and automatically assign a SUCCESS STATUS (200) */
void send_message(Request &request, ClientSocket& targetClient){
    auto json_ptr_data = new json(request.data);
    Response response(SUCCESS, RES_MESSAGE, SERVER_NAME, *json_ptr_data, json_ptr_data);
    socket_send(response, targetClient);
}

/* Send a pre-constructed request of type REQ_ORDER to a target client. This will transform the request
 * into a response and automatically assign a SUCCESS STATUS (200) */
void send_order(Request &request, ClientSocket& targetClient){
    auto json_ptr_data = new json(request.data);
    Response response(SUCCESS, RES_ORDER, SERVER_NAME, *json_ptr_data, json_ptr_data);
    socket_send(response, targetClient);
}

/* Send a pre-constructed request of type REQ_BAKER_STATUS to a target client. This will transform the request
 * into a response and automatically assign a SUCCESS STATUS (200) */
void send_status_update(Request &request, ClientSocket& targetClient){
    auto json_ptr_data = new json(request.data);
    Response response(SUCCESS, RES_BAKER_STATUS, SERVER_NAME, *json_ptr_data, json_ptr_data);
    socket_send(response, targetClient);
}

/* Forwards any request with a valid REQ_ type to the given target client with the correct helper method.
 * Use this method over the send_xxx methods. */
void forward_as_response(Request &request, ClientSocket& targetClient){
    switch(request.getType()){
        case REQ_BAKER_STATUS: {
            send_status_update(request, targetClient);
            return;
        }
        // When the server forwards complete baking to delivery
        case REQ_BAKING_COMPLETE:
        case REQ_ORDER: {
            send_order(request, targetClient);
            return;
        }
        case REQ_MESSAGE:{
            send_message(request, targetClient);
            return;
        }
        default:{
            std::cout << "Message not forwarded, type not recognized" << std::endl;
            return;
        }
    }
}

/* Forwards a customer order to a PIZZA_BAKER client if any are available. */
void process_customer_order(Request &request, ClientSocket &clientSocket, ClientManager &clients, FileWriter &fw){
    bool pizza_baker_found = false;
    clients.lock_client();
    for (auto& client: clients.clients)
    {
        // TODO: Possibly specify the baker target specifically, as of now all sockets connected as baker receive the job
        if (client.second->socket_type == PIZZA_BAKER)
        {
            forward_as_response(request, *client.second);
            std::cout << "Data forwarded for baking to " << client.second->name << "." << std::endl;
            pizza_baker_found = true;
        }
    }
    clients.unlock_client();

    if (!pizza_baker_found)
    {
        std::cout << "ALL THE PIZZA BAKERS ARE SLEEPING?! FIRED!" << std::endl;
        send_new_message("All baking outlets are offline", ERROR, clientSocket);
    }
}

/* Forwards a PIZZA_BAKER's status update to a client socket of type PIZZA_MAKER which matches the order id
 * received from the pizza baker's request body */
void notify_customer_of_pizza_status(Request &request, ClientSocket &clientSocket, ClientManager &clients) {
    std::string order_id = parse_customer_target(request);
    ClientSocket* customer_socket = find_order_associated_socket(order_id, clients);
    if (customer_socket == nullptr)
    {
        send_new_message("The customer has disconnected and will not receive the message",
                         SUCCESS, clientSocket);
        std::cout << "Customer offline" << std::endl;
        return;
    }
    request.header.type = RES_BAKER_STATUS;
    forward_as_response(request, *customer_socket);
}


/* Handles all socket related processing of a client socket with type PIZZA_MAKER */
void pizza_maker_handler(Request *request, ClientSocket &clientSocket, ClientManager &clients, FileWriter &fw)
{
    Request* new_request = nullptr;
    std::cout << "In pizza maker handler" << std::endl;
    RequestTypes request_type = request->getType();
    switch(request_type){
        case REQ_ORDER:{
            std::string order_id;
            new_request = update_order_id(request, fw, order_id);
            std::string msg = "Order received. Your order ID is " + order_id;
            clientSocket.order_id.push_back(order_id);
            send_new_message(msg, SUCCESS, clientSocket);
            process_customer_order(*new_request, clientSocket, clients, fw);
            break;
        }
        default:{
            send_new_message("Invalid request type.", ERROR, clientSocket);
            std::cout << "Invalid request made by pizza customer connected as "+clientSocket.name << std::endl;
            break;
        }
    }

    delete new_request;
}


void notify_customer_of_delivery(Request &request, ClientManager &clients){
    std::string order_id = parse_customer_target(request);
    ClientSocket* customer_socket = find_order_associated_socket(order_id, clients);

    if (customer_socket == nullptr)
    {
        std::cout << "Customer offline" << std::endl;
        return;
    }
    send_new_message("Your order has been dispatched for delivery!", SUCCESS, *customer_socket);
}

/*
 * Forwards a completed pizza order from a PIZZA_BAKER to a PIZZA_DELIVERY client socket.
 */
void send_to_delivery(Request &request, ClientSocket &clientSocket, ClientManager &clients) {
    bool found_deliverer = false;
    clients.lock_client();
    for (auto& client: clients.clients)
    {
        if (client.second->socket_type == PIZZA_DELIVERER)
        {
            forward_as_response(request, *client.second);
            std::cout << "Data forwarded for delivery " << client.second->name << "." << std::endl;
            found_deliverer = true;
        }
    }
    clients.unlock_client();

    if (!found_deliverer)
    {
        std::cout << "All pizza deliverers are offline." << std::endl;
        return;
    }
    notify_customer_of_delivery(request, clients);
    //notify_customer_of_pizza_status(request, clientSocket,clients);
}

/* Saves a completed order to a file and notifies the socket that sent in the completed order of it's success */
void save_completed_order(ClientSocket &clientSocket, FileWriter &fw, PizzaOrder *pizza_order) {
    pizza_order->complete = true;                               // Make sure it's marked as completed
    json json_pizza_order;
    PizzaOrder::to_json(*pizza_order, json_pizza_order);  // Get it on json format
    fw.save_completed_order(json_pizza_order.dump());    // Dump the json to string and write to file
    send_new_message("Pizza order saved as complete!", SUCCESS, clientSocket);
    delete pizza_order;
}

/* Handles all socket related processing of a client socket with type PIZZA_BAKER */
void pizza_baker_handler(Request* request, ClientSocket &clientSocket, ClientManager &clients, FileWriter &fw)
{
    switch(request->getType())
    {
        case REQ_BAKER_STATUS:{
            // The request was a status update that a customer should receive
            notify_customer_of_pizza_status(*request, clientSocket, clients);
            return;
        }
        case REQ_BAKING_COMPLETE: {
            PizzaOrder* pizza_order = parse_order(*request);             // get pizza order as object
            if (pizza_order->complete || !pizza_order->delivery ){
                save_completed_order(clientSocket, fw, pizza_order); // Save as there is no delivery
                notify_customer_of_pizza_status(*request, clientSocket, clients);
            }
            else send_to_delivery(*request, clientSocket, clients);                // Send to delivery otherwise
            return;

        }
        default:{
            send_new_message("Invalid data type in request.", ERROR, clientSocket);
            std::cout << "Invalid baker request received" << std::endl;
            return;
        }
    }
}

/* Handles all socket related processing of a client socket with type PIZZA_DELIVERER */
void pizza_deliverer_handler(Request* request, ClientSocket& client, FileWriter &fr)
{
    switch(request->getType()){
        case REQ_DELIVERY_COMPLETE:{
            PizzaOrder* pizza_order = parse_order(*request);   // Get pizza order as object
            save_completed_order(client, fr, pizza_order); // Save to file
            break;
        }
        default:{
            std::cout << "Deliverer sent invalid data type" << std::endl;
            send_new_message("Invalid data type sent to server", ERROR, client);
            break;
        }
    }
}

/* Routes a connecting socket according to the socket type received from the socket handshake */
void dataProcessingRouter(Request* request, ClientSocket& clientSocket, ClientManager &clients, FileWriter& fr)
{
    switch(clientSocket.socket_type){
        case PIZZA_MAKER:
            pizza_maker_handler(request, clientSocket, clients, fr);
            break;
        case PIZZA_BAKER:
            pizza_baker_handler(request, clientSocket, clients, fr);
            break;
        case PIZZA_DELIVERER:
            pizza_deliverer_handler(request, clientSocket, fr);
            break;
        default:{
            std::cout << "Invalid socket type in data forwarding" << std::endl;
            return;
        }
    }
}

// TODO: Make a handshake timeout to terminate socket listener
/*
 * Handshake is the initial communication between the server and a client.
 * The server awaits the client to send a request that specifies which type of
 * socket it wishes to connect as. (SocketTypes enum)
 */
void makeHandshake(ClientSocket &client) {
    char socket_type_buffer[SERVER_BUFFER_LIMIT];

    int receive_data_size = recv(client.socket, socket_type_buffer, SERVER_BUFFER_LIMIT, 0);
    if (receive_data_size <= 0){
        std::cout << "Error in handshake" << std::endl;
        return;
    }
    std::string data(socket_type_buffer);                           // Get request as string
    Request* request = Request::parse(data);                        // Parse request to object
    auto socket_type = SocketTypes(parse_handshake(*request));  // Retrieve the socket type from request

    switch (socket_type) {
        case INVALID_SOCKET_TYPE:
        case UNASSIGNED:{
            std::cout << "Handshake failed with client socket. The socket type was invalid" << std::endl;
            send_new_message("Handshake failed! Received an invalid type of socket.", ERROR, client);
            return;
        }
        case PIZZA_MAKER:{
            std::cout << "Handshake was successful! Socket recognized as PIZZA MAKER" << std::endl;
            client.socket_type = socket_type;
            send_new_message("Server handshake successful!", HANDSHAKE_SUCCESS, client);
            return;
        }
        case PIZZA_BAKER:{
            std::cout << "Handshake was successful! Socket recognized as PIZZA BAKER" << std::endl;
            client.socket_type = socket_type;
            send_new_message("Server handshake successful!", HANDSHAKE_SUCCESS, client);
            return;
        }
        case PIZZA_DELIVERER:{
            std::cout << "Handshake was successful! Socket recognized as PIZZA DELIVERER" << std::endl;
            client.socket_type = socket_type;
            send_new_message("Server handshake successful!", HANDSHAKE_SUCCESS, client);
            return;
        }
        default:{
            std::cout << "Handshake failed with client socket. The socket type was invalid" << std::endl;
            send_new_message("Handshake failed! Received an invalid type of socket.", ERROR, client);
            return;
        }
    }

}

/* Initial entry of all client sockets. Get's routed according to handshake response. */
void listenSocket(SOCKET_TYPE client_socket, int &client_ids, ClientManager &clients,
                  std::mutex& cli_mutex, FileWriter& fr,
                  std::vector<std::pair<bool,std::thread*>>& threads, int thread_index, std::mutex &thread_mod_mutex)
{
    cli_mutex.lock();           // Lock the mutex reference received as server class instance member
    client_ids++;               // Add to the reference int to notify server class instance
    int client_id = client_ids; // Set the client id of the new client
    cli_mutex.unlock();         // Unlock the reference mutex

    auto* new_client_socket = new ClientSocket(client_id, UNASSIGNED, client_socket);
    makeHandshake(*new_client_socket);

    if (new_client_socket->socket_type == UNASSIGNED || new_client_socket->socket_type == INVALID_SOCKET_TYPE)
    {
        std::cout << "Terminating "+new_client_socket->name+". Handshake failed." << std::endl;
        return;
    }

    clients.add_client(client_id, new_client_socket); // Client manager contains all connected sockets

    while(true)
    {
        char buffer[SERVER_BUFFER_LIMIT] = {'\0'};
        int received_data_size = recv(client_socket, buffer, SERVER_BUFFER_LIMIT, 0);
        std::cout << "Server received a message from " << new_client_socket->name << std::endl;
        if (received_data_size <= 0) {
            std::cout << new_client_socket->name << " disconnected" << std::endl;
            break; // End thread
        }

        std::string data(buffer);
        Request* request = Request::parse(data);
        dataProcessingRouter(request, *new_client_socket, clients, fr);
        delete request;
    }
    clients.remove_client(client_id);    // Use manager's lock guarded method to remove a client
    thread_mod_mutex.lock();             // Lock thread mutex to ensure no modifications happen while removing
    threads[thread_index].first = false; // Notify server that thread is inactive
    thread_mod_mutex.unlock();           // Unlock when done
}

[[noreturn]] Server::Server() {
    FileWriter fileWriter; // File writer is shared by all threads and is lock protected
    auto time = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch());
    time_since_last_cleanup = time.count(); // Used to remove inactive threads on an interval

    std::mutex cli_idx_mutex;      // Mutex to modify the class member variable client_ids
    std::mutex thread_state_mutex; // Mutes to modify the class member threads

    TCP tcp;                       // Socket Setup helper

    if (!tcp.check_and_start_if_windows())
        throw std::runtime_error("Windows os failed socket creation");

    SOCKET_TYPE s_socket = socket(AF_INET, SOCK_STREAM, 0);
    sockaddr socket_address;
    tcp.make_socket_address(&socket_address, TCP_PORT);

    if (bind(s_socket, &socket_address, sizeof(socket_address)) < 0)
    {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    listen(s_socket, 5);
    ADDRESS_SIZE addr_size = sizeof(socket_address);
    std::cout << "Server is running." << std::endl;
    while(true)
    {
        SOCKET_TYPE new_client_socket;
        new_client_socket = accept(s_socket, &socket_address, &addr_size);
        std::cout << "New client connected!" << std::endl;
        thread_state_mutex.lock(); // lock the mutex until we've actually added the vector pair to the threads vector
        auto* new_thread = new std::thread(listenSocket,
                                           std::ref(new_client_socket),
                                           std::ref(client_ids),
                                           std::ref(clientManager),
                                           std::ref(cli_idx_mutex),
                                           std::ref(fileWriter),
                                           std::ref(threads),
                                           threads.size(),
                                           std::ref(thread_state_mutex));
        threads.emplace_back(true, new_thread);
        thread_state_mutex.unlock();
        // Clean up threads every 30 seconds if necessary
        if (time.count() > CLEAN_UP_INTERVAL + time_since_last_cleanup){
            std::cout << "Elapsed time: " << time.count()-time_since_last_cleanup << std::endl;
            clean_up(thread_state_mutex);
        }
        time = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch());
    }
}

void Server::clean_up(std::mutex &thread_state_mutex) {
    std::cout << "Preforming cleanup" << std::endl;
    int count = 0;
    std::vector<int> threads_to_remove = {};
    thread_state_mutex.lock();
    for (auto thread: threads)
    {
        if (!thread.first && thread.second->joinable())
        {
            thread.second->join();
            std::cout << "Deleting an inactive thread" << std::endl;
            delete thread.second;
            threads_to_remove.push_back(count);
        }
    }
    for (int to_remove: threads_to_remove)
        threads.erase(threads.begin()+to_remove);
    thread_state_mutex.unlock();
    time_since_last_cleanup = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch()).count();
}

