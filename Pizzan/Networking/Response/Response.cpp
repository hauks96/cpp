#include "Response.h"
#include "../../json.hpp"
#include <iostream>
using json = nlohmann::json;

/* Constructor for all other use cases */
Response::Response(ResponseStatus status, ResponseTypes type, const std::string &sender, const json &data):
    data(data), header(ResponseHeader(status, type, sender)){}

/* Constructor for internal parse method */
Response::Response(ResponseStatus status, ResponseTypes type, const std::string &sender, const json &data,json::pointer j_ptr):
        data(data), header(ResponseHeader(status, type, sender)), j_prt(j_ptr){}

/* Dumps a response object to string to send between sockets */
std::string Response::dump() {
    json response;
    response["ResponseHeader"] = json(header);
    response["body"] = json(data);
    std::string dump = response.dump();
    return dump;
}

/* Parses a stringified response to a response object */
Response* Response::parse(const std::string &dumped_response) {
    json response = json::parse(dumped_response);
    ResponseHeader responseHeader = response["ResponseHeader"].get<ResponseHeader>();
    auto new_body = new json(response["body"]);
    auto* new_response = new Response(ResponseStatus(responseHeader.status),
                                      ResponseTypes(responseHeader.type),
                                      responseHeader.sender, *new_body, new_body);
    return new_response;
}

/* Returns the status of the response object */
ResponseStatus Response::getStatus() const {
    return ResponseStatus(header.status);
}

/* Returns the type of the response object */
ResponseTypes Response::getType() const {
    return ResponseTypes(header.type);
}

Response::~Response() {
    delete j_prt;
}

