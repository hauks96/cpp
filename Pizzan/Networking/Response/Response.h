#ifndef PIZZAN_RESPONSE_H
#define PIZZAN_RESPONSE_H
#include "ResponseStatusesEnum.h"
#include "ResponseHeader.h"
#include "ResponseTypesEnum.h"
#include "../../json.hpp"
using json = nlohmann::json;


class Response {
public:
    /*
     * The data inserted can be retrieved on the other end after parsing by getting json at key "body".
     * Before deserializing anything the response header should be read
     */
    explicit Response(ResponseStatus status, ResponseTypes type, const std::string &sender, const json &data);
    explicit Response(ResponseStatus status, ResponseTypes type, const std::string &sender, const json &data, json::pointer j_ptr);
    ~Response();
    const json &data;
    ResponseHeader header;
    // Returns json as string. Contains the keys "ResponseHeader" and "body" when parsed to json
    std::string dump();
    ResponseStatus getStatus() const;
    ResponseTypes getType() const;
    static Response* parse(const std::string &dumped_request);

private:
    json::pointer j_prt = nullptr;
};

#endif //PIZZAN_RESPONSE_H
