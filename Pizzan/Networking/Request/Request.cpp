#include "Request.h"
#include <iostream>
Request::Request(RequestTypes type, std::string sender, const json &data):
    data(data), header(RequestHeader(type, sender)){}

Request::Request(RequestTypes type, std::string sender, const json &data, json::pointer j_prt):
        data(data), header(RequestHeader(type, sender)), j_prt(j_prt) {}

std::string Request::dump() {
    json request;
    request["RequestHeader"] = json(header);
    request["body"] = json(data);
    std::string dump = request.dump();
    return dump;
}

Request *Request::parse(const std::string &dumped_request) {
    json request = json::parse(dumped_request);
    json request_header = json(request["RequestHeader"]);
    RequestHeader requestHeader = request["RequestHeader"].get<RequestHeader>();
    auto request_body = new json(request["body"]);
    auto* new_request = new Request(RequestTypes(requestHeader.type), requestHeader.sender, *request_body, request_body);
    return new_request;
}

RequestTypes Request::getType() const {
    return RequestTypes(header.type);
}

Request::~Request() {
    delete j_prt;
}


