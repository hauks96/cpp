#ifndef PIZZAN_REQUESTTYPESENUM_H
#define PIZZAN_REQUESTTYPESENUM_H

enum RequestTypes {
    REQ_HANDSHAKE, // data["body"]["handshake"]
    REQ_MESSAGE, // data["body"]["message"]
    REQ_BAKER_STATUS, // data["body"]["message"], data["body"]["orderID"]
    REQ_ORDER, // data["order"]
    REQ_BAKING_COMPLETE,
    REQ_DELIVERY_COMPLETE,
};
#endif //PIZZAN_REQUESTTYPESENUM_H
