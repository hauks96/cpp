#ifndef PIZZAN_TCP_H
#define PIZZAN_TCP_H

#include <iostream>
#include <string>
#include <thread>

#ifdef _WIN32
#define MY_OS "Windows"
#include <winsock2.h>
typedef SOCKET SOCKET_TYPE;
typedef int ADDRESS_SIZE;
#define ON_WINDOWS
#else
#define MY_OS "NOT Windows"
    typedef int SOCKET_TYPE;
    typedef unsigned int ADDRESS_SIZE;
    #include <sys/socket.h>
    #include <netinet/in.h>
    #include <arpa/inet.h>
    #include <errno.h>
#endif

#define TCP_PORT 3500 //IPEN TCP PORTS ON SKEL: 4000-4100
#define UDP_PORT 4101 //IPEN UDP PORTS ON SKEL: 4100-4200

class TCP {
public:
    TCP() = default;
    bool check_and_start_if_windows();
    void make_socket_address(sockaddr *address, int port, std::string ip_address = "");
    bool connect_to_server(SOCKET_TYPE &my_socket, sockaddr &socket_address);
};


#endif //PIZZAN_TCP_H
