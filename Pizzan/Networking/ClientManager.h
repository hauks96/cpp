#ifndef PIZZAN_CLIENTMANAGER_H
#define PIZZAN_CLIENTMANAGER_H
#include "ClientSocket.h"
#include <mutex>
class ClientManager {
public:
    ClientManager() = default;
    std::unordered_map<int, ClientSocket*> clients = {};
    // Lock protected addition to the client list
    void add_client(int client_id, ClientSocket* client_socket) {
        std::lock_guard<std::mutex> guard(modification_mutex);
        clients.insert(std::make_pair(client_id, client_socket));
    }
    // Lock protected removal from the client list
    void remove_client(int client_id){
        std::lock_guard<std::mutex> guard(modification_mutex);
        delete clients[client_id];
        clients.erase(client_id);
    }
    // Allows manual locking of the client list
    void lock_client(){
        modification_mutex.lock();
    }
    // Allows manual unlocking of the client list
    void unlock_client(){
        modification_mutex.unlock();
    }

protected:
    std::mutex modification_mutex;
};

#endif //PIZZAN_CLIENTMANAGER_H
