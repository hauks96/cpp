#include "FileWriter.h"
#include <fstream>
#include <iostream>
#include <mutex>
#define COMPLETED_DELIVERIES_FILE "Data/CompletedDeliveries/completed_deliveries.txt"
#define COMPLETED_ORDERS_FILE "Data/CompletedOrders/completed_orders.txt"
#define COMPLETED_BAKINGS_FILE "Data/CompletedBakings/completed_bakings.txt"
#define CREATED_ORDERS_FILE "Data/CreatedOrders/created_orders.txt"
#define ORDER_ID_COUNTER_FILE "Data/CompletedOrders/order_id_counter.txt"

void FileWriter::save_completed_order(const std::string& order) {
    append_to_file(COMPLETED_ORDERS_FILE, order);
    std::cout << "Completed order saved in file" << std::endl;
}

void FileWriter::save_completed_delivery(const std::string& order) {
    append_to_file(COMPLETED_DELIVERIES_FILE, order);
    std::cout << "Completed delivery saved in file" << std::endl;
}

void FileWriter::save_completed_baking(const std::string &order) {
    append_to_file(COMPLETED_BAKINGS_FILE, order);
    std::cout << "Completed baking saved in file" << std::endl;
}

void FileWriter::save_created_order(const std::string &order) {
    append_to_file(CREATED_ORDERS_FILE, order);
    std::cout << "Created order saved in file" << std::endl;
}

void FileWriter::append_to_file(const std::string &filename, const std::string &data) {
    write_mutex.lock();
    std::ofstream out(filename, std::ios::app);
    if (!out.is_open()){
        std::cout << "Target file: " << filename << std::endl;
        throw std::runtime_error("File not found");
    }

    out << data << std::endl;
    out.close();
    write_mutex.unlock();
}
// TODO: Use get next order to assign order id's on server. Add it to the order before sending to baker. Then send that order id to the customer with a message.
int FileWriter::get_next_order_id() {
    std::lock_guard<std::mutex> guard(order_id_write_mutex);
    // fstream wasn't working
    std::ifstream in(ORDER_ID_COUNTER_FILE);
    if (!in.is_open())throw std::runtime_error("ORDER_ID file not found");

    int curr_order_id = 0;
    in >> curr_order_id;
    in.close();

    curr_order_id++;

    std::ofstream out(ORDER_ID_COUNTER_FILE);
    if(!out.is_open()) throw std::runtime_error("ORDER_ID file not found");

    out << curr_order_id;
    out.close();
    return curr_order_id;
}
