#ifndef PIZZAN_FILEWRITER_H
#define PIZZAN_FILEWRITER_H
#include <string>
#include <mutex>
class FileWriter {
public:
    FileWriter() = default;
    void save_completed_order(const std::string& order);
    void save_completed_delivery(const std::string& order);
    void save_completed_baking(const std::string& order);
    void save_created_order(const std::string& order);
    void append_to_file(const std::string& filename, const std::string& data);
    int get_next_order_id();
protected:
    std::mutex write_mutex;
    std::mutex order_id_write_mutex;
private:
    int order_id;
};

#endif //PIZZAN_FILEWRITER_H
