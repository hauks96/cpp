#ifndef PIZZAN_PIZZAMAKER_H
#define PIZZAN_PIZZAMAKER_H
#include "../Serializers/Pizza.h"
#include "../Serializers/Size.h"
#include "../Serializers/Bottom.h"
#include "../Serializers/Topping.h"
#include "../Serializers/ToppingType.h"
#include <vector>
#include <map>
/*
 * For creating a pizza.
 * This is though of as an api for customer pizza creation
 */
class PizzaMaker {
public:
    PizzaMaker();
    Pizza& get_pizza();
    std::vector<Pizza*> get_pizzas();
    void new_pizza();
    int get_price();
    int get_total_price();
    void new_pizza(Pizza& template_pizza);
    void remove_pizza(int pizza_index);
    void select_pizza(int pizza_index); // Select one of the pizzas the user has created
    void select_size(int size_id);
    void select_bottom(int bottom_id);
    void select_topping(int type_id, int topping_id);
    void remove_topping(int type_id, int topping_id);
    void clear();

    std::map<int, Size>        sizes             = {};
    std::map<int, Bottom>      bottoms           = {};
    std::map<int, Topping>     toppings          = {};
    std::map<int, Pizza*>      pizzas            = {};
    std::map<int, ToppingType> toppingTypes      = {};

protected:
    bool validate_current_pizza();
    void calculate_pizza_price();

    int get_topping_incrementer(int size_id);

    template<class T>
    bool validate(std::map<int, T>& list, int validation);

    template<class T>
    int get_price(std::map<int, T> &list, int identifier);

private:
    Pizza*                               current_pizza     = nullptr;
    int                                  current_pizza_idx = -1;

    void remove_or_decrease(int type_id, int topping_id);
    void create_or_increase(int type_id, int topping_id);
};

#endif //PIZZAN_PIZZAMAKER_H
