#ifndef PIZZAN_PIZZAPRINTER_H
#define PIZZAN_PIZZAPRINTER_H
#include "../Serializers/Pizza.h"
#include "../Serializers/Size.h"
#include "../Serializers/Bottom.h"
#include "../Serializers/Topping.h"
#include "../Serializers/ToppingType.h"
#include <map>
#include <vector>
/*
 * Prints pizzas to the terminal
 */
class PizzaPrinter {
public:
    PizzaPrinter();
    void print(Pizza& pizza);
    void print(const std::vector<Pizza*>& pizzas);

protected:
    template<class T>
    std::string get_name(std::map<int, T> &list, int identifier);

    void printSize(int size_id);
    void printTopping(int topping_type, int topping_id, int topping_amount);
    void printBottom(int bottom_id);

private:
    std::map<int, Size>        sizes        = {};
    std::map<int, Bottom>      bottoms      = {};
    std::map<int, Topping>     toppings     = {};
    std::map<int, ToppingType> toppingTypes = {};

    void printToppingType(int type_id);
};

#endif //PIZZAN_PIZZAPRINTER_H
