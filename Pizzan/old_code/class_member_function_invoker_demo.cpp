#include <functional>
#include <iostream>
using namespace std::placeholders;

class SomeInterface {
public:
    // Some variable that needs to be updated once a server response is received
    int some_var = -5;

    void response_handler(int num){
        some_var+=num;
        std::cout << "In response handler function!" << std::endl;
        another();
    }
    void another(){
        std::cout << "Another got called" << std::endl;
        some_var += 5;
    }
};

class ClientListener {
public:
    // Takes a bound function pointer that gets triggered in one of it's own methods
    ClientListener(std::function<void(int)> handler){
        process_function = handler;
    }
    std::function<void(int)> process_function;
    // Method that gets called when server gives a response
    void server_response_processor(int server_response){
        process_function(server_response);
    }
};


int main()
{
    SomeInterface test;
    // Binding an instance method to a function pointer (_1 means takes on argument)
    auto function_pointer = std::bind(&SomeInterface::response_handler, &test, _1);
    ClientListener client_listener(function_pointer);
    std::cout << "some_var before server response method: " <<  test.some_var << std::endl;
    client_listener.server_response_processor(10); // Method that would be invoked on server response
    std::cout << "some_var after server response method: " << test.some_var << std::endl; // Showing that the variable changed
}