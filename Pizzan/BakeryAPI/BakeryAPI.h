#ifndef PIZZAN_BAKERYAPI_H
#define PIZZAN_BAKERYAPI_H


#include "../Networking/Request/Request.h"
#include "../Networking/Response/Response.h"
#include "../Networking/Client/Client.h"
#include "../Networking/SocketTypesEnum.h"
#include "../PizzaBaker/PizzaBaker.h"
#include "../PizzaPrinter/PizzaPrinter.h"
#include <mutex>

class BakeryAPI {
public:
    void server_response_handler(Response* response);
    void start();
    void add_client(Client* client_socket);
    void print_orders();
    std::mutex terminal_mutex;
protected:
    static void print_command_header();
    static void print_list_header();
    static void format_list_element(const std::string& b_id, const std::string& or_id, const std::string& cus_name, const std::string& o_time, const std::string& o_status);
    void print_order_details(int order_id);
    bool get_command_input();
    void send_status_update(BakerOrder& baker_order);
    void notify_completion(BakerOrder& baker_order);

    // Set to true when user is interacting with terminal to notify an update from server after user is done using window
    bool using_window = false;
    PizzaPrinter pizzaPrinter;
    PizzaBaker pizzaBaker;
    Client* client;

    void update_order();
    void order_details();
};


#endif //PIZZAN_BAKERYAPI_H
