#include "PizzaDeliverer.h"
#include "../FileWriter/FileWriter.h"

#define COMPLETED_DELIVERY_DECAY_TIME 60


void PizzaDeliverer::add_order(PizzaOrder *pizza_order) {
    std::vector<int> to_remove = {};
    long int time_now = std::chrono::duration_cast<std::chrono::minutes>(std::chrono::system_clock::now().time_since_epoch()).count();
    for (auto order: orders)
    {
        if (order.second->getState() == DELIVERED)
        {
            long int time_diff = time_now - std::chrono::duration_cast<std::chrono::minutes>(order.second->time.time_since_epoch()).count();
            if (time_diff>COMPLETED_DELIVERY_DECAY_TIME*60000){
                to_remove.push_back(order.first);
            }
        }
    }
    for (auto id: to_remove) remove_order(id);
    orders.insert({next_id, new DeliveryOrder(pizza_order, ORDER_RECEIVED)});
    next_id++;
}

void PizzaDeliverer::remove_order(int order_id) {
    if (exists(order_id))
    {
        DeliveryOrder* order = orders[order_id];
        delete order;
        orders.erase(order_id);
        return;
    }
}

DeliveryStates get_next_state(DeliveryStates curr_state)
{
    switch(curr_state)
    {
        case ORDER_RECEIVED:
        case DELIVERED:
            return DELIVERED;
        default:
            return ORDER_RECEIVED;
    }
}


bool PizzaDeliverer::update_order_status(int order_id) {
    if (exists(order_id))
    {
        DeliveryOrder* delivery_order = orders[order_id];
        delivery_order->update_status(get_next_state(delivery_order->getState()));
        if (delivery_order->getState() == DELIVERED)
        {
            json json_order;
            PizzaOrder::to_json(*delivery_order->order, json_order);
            std::string str_order = json_order.dump();
            FileWriter fw;
            fw.save_completed_delivery(str_order);
        }
        return true;
    }
    return false;
}

bool PizzaDeliverer::exists(int id) const {
    return orders.count(id)>0;
}

DeliveryOrder *PizzaDeliverer::get_order(int order_id) {
    if (exists(order_id))
    {
        return orders[order_id];
    }
    return nullptr;
}

DeliveryStates PizzaDeliverer::get_order_state(int order_id) {
    if (exists(order_id))
        return orders[order_id]->getState();
    return ORDER_RECEIVED;
}
