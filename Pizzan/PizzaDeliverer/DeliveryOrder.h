#ifndef PIZZAN_DELIVERYORDER_H
#define PIZZAN_DELIVERYORDER_H
#include "../Serializers/PizzaOrder.h"
#include "DeliveryStates.h"
#include "../date.h"

class DeliveryOrder {
public:
    DeliveryOrder(PizzaOrder* order, DeliveryStates state):
            order(order), state(state){
        time = std::chrono::system_clock::now();
        auto minute_precision_time = date::floor<std::chrono::minutes>(time);
        time_string = date::format("%Y-%m-%d %H:%M", minute_precision_time);
        update_status(state);
    }

    ~DeliveryOrder()
    {
        for (auto pizza: order->pizzas)
            delete pizza;
    }

    void update_status(DeliveryStates new_status){
        state = new_status;
        switch(new_status)
        {
            case ORDER_RECEIVED:
                status_string = "ORDER_RECEIVED";
                break;
            case DELIVERED:
                status_string = "DELIVERED";
                break;
            default:
                status_string = "ORDER RECEIVED";
                break;
        }
    }

    std::chrono::time_point<std::chrono::system_clock> time;
    std::string time_string;
    std::string status_string;
    PizzaOrder* order;
    int state;

    DeliveryStates getState() const
    {
        return DeliveryStates(state);
    }
};


#endif //PIZZAN_DELIVERYORDER_H
