#ifndef PIZZAN_PIZZADELIVERER_H
#define PIZZAN_PIZZADELIVERER_H
#include "DeliveryOrder.h"
#include <map>
#include <string>
#include "../Serializers/PizzaOrder.h"

class PizzaDeliverer {
public:
    PizzaDeliverer() = default;
    // id (int assigned by class), BakerOrder*
    std::map<int, DeliveryOrder*> orders = {};
    /* Automatically removes orders that have decayed when new ones arrive */
    void add_order(PizzaOrder* pizza_order);
    /* Deallocates the memory */
    void remove_order(int order_id);
    /* Automatically saves completed orders*/
    bool update_order_status(int order_id);

    bool exists(int id) const;

    DeliveryOrder* get_order(int order_id);
    DeliveryStates get_order_state(int order_id);

private:
    int next_id = 0;
};


#endif //PIZZAN_PIZZADELIVERER_H
