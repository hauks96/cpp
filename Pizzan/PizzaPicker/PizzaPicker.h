#ifndef PIZZAN_PIZZAPICKER_H
#define PIZZAN_PIZZAPICKER_H
#include "../Serializers/Pizza.h"
#include <map>
#include <vector>
/*
 * To select a pizza from menu.
 * The selected pizza can then be inserted into to PizzaMaker to customize.
 */
class PizzaPicker {
public:
    PizzaPicker();
    std::vector<Pizza*> get_pizzas();
    Pizza& select_pizza(int pizza_id);
private:
    std::map<int, Pizza*> pizzas = {};
};


#endif //PIZZAN_PIZZAPICKER_H
