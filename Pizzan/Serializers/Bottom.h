#ifndef PIZZAN_BOTTOM_H
#define PIZZAN_BOTTOM_H
#include <string>
#include "../json.hpp"
using json = nlohmann::json;

namespace bottom_serializer {
    struct Bottom {
        int id;
        int price;
        std::string name;
    };
    NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(Bottom, id, price, name)
}

using namespace bottom_serializer;

#endif //PIZZAN_BOTTOM_H
