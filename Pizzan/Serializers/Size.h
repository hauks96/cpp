#ifndef PIZZAN_SIZE_H
#define PIZZAN_SIZE_H
#include <string>
#include "../json.hpp"
using json = nlohmann::json;

namespace size_serializer {
    struct Size {
        int id;
        int price;
        int circumference;
        int topping_price_incrementer;
        std::string name;
    };
    NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(Size, id, price, circumference, topping_price_incrementer, name)
}

using namespace size_serializer;

#endif //PIZZAN_SIZE_H
