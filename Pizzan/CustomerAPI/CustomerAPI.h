#ifndef PIZZAN_CUSTOMERAPI_H
#define PIZZAN_CUSTOMERAPI_H

#include "../Serializers/PizzaOrder.h"
#include "../PizzaMaker/PizzaMaker.h"
#include "../PizzaPicker/PizzaPicker.h"
#include "../PizzaPrinter/PizzaPrinter.h"
#include "../Networking/Response/Response.h"
#include "../Networking/Request/Request.h"
#include "../Networking/Client/Client.h"
#include "../Networking/SocketTypesEnum.h"
#include <mutex>

class CustomerAPI {
public:
    CustomerAPI();
    void start();
    void server_response_handler(Response* response);
    template<class T>
    int terminal_helper (std::map<int, T> &options_to_print, const std::string& menu_name);
    void print_header(const std::string& menu_name);
    template <class T>
    void print_list(std::map<int, T> options_to_print);
    void add_client(Client* client);
    void print_to_terminal(const std::string& message);
    std::mutex terminal_mutex;

protected:
    bool main();
    void complete_order();

    void new_pizza();
    bool new_pizza_selection();
    bool are_you_sure(const std::string &sure_of_what);

    bool edit_pizza();
    bool edit_size();
    bool edit_bottom();
    bool add_topping();
    bool add_topping_of_type(int type);
    bool delete_topping();

    bool delivery_options();
    bool pickup_form();
    bool delivery_form();

    bool remove_pizza();
    bool select_created_pizza();
    int select_create_helper();
    void order_overview();

    static int get_number_input();

    Client* client = nullptr;
    bool order_complete = false;

    bool deliver      = false;
    bool pui_complete = false;
    bool di_complete  = false;
    PickUpInfo* pickUpInfo     = new PickUpInfo;
    DeliveryInfo* deliveryInfo = new DeliveryInfo;

    PizzaPicker pizzaPicker;
    PizzaMaker pizzaMaker;
    PizzaPrinter pizzaPrinter;

    void press_any_to_continue();
};


#endif //PIZZAN_CUSTOMERAPI_H
