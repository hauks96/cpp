#include "CustomerAPI.h"
#include <functional>
#include <cstdlib>
#include <iostream>
#include <map>
#include <sstream>
#include "Option.h"
#include "../Networking/Parser/Parser.h"
#include "../json.hpp"
using nlohmann::json;
using namespace std::placeholders;


int CustomerAPI::get_number_input()
{
    std::string input;
    int number = 0;
    while(true)
    {
        getline(std::cin, input);
        std::stringstream str_stream(input);
        if (str_stream >> number) break;
        std::cout << "Not a number." << std::endl;
    }
    return number;
}

void CustomerAPI::press_any_to_continue(){
    print_to_terminal("Press enter to continue...");
    std::string input;
    getline(std::cin, input);
}

template <class T>
/* DO NOT USE THIS METHOD FOR THE PIZZAS THEY ARE POINTERS
 * This prints a selection of options to the terminal
 * */
void CustomerAPI::print_list(std::map<int, T> options_to_print){
    int count = 1;
    for (auto& element: options_to_print)
    {
        print_to_terminal(" "+std::to_string(count)+". "+element.second.name);
        count++;
    }
}

void CustomerAPI::print_header(const std::string& menu_name){
    // Some printing to terminal
    std::lock_guard<std::mutex> guard(terminal_mutex);
    std::cout << std::endl << " --- " << menu_name << " --- " << std::endl << std::endl;
    std::cout << "-1. " << "Back" << std::endl;
}

template<class T>
/* DO NOT USE THIS METHOD FOR THE PIZZAS THEY ARE POINTERS
 * Returns the identifier of the selected option according to the json files
 * */
int CustomerAPI::terminal_helper (std::map<int, T> &options_to_print, const std::string& menu_name){
    // Map the options to numbers starting at 1
    std::map<int, int> option_mapper = {};
    int count = 1;
    for (const auto& idf: options_to_print)
    {
        option_mapper[count] = idf.first;
        count++;
    }
    print_header(menu_name);
    print_list(options_to_print);
    print_to_terminal("Enter the number you wish to select: ");

    while(true){
        int ret_num = get_number_input();

        if (option_mapper.count(ret_num)>0){
            return option_mapper[ret_num];  // Return the value if it exists
        } // Check if the option selected is valid
        if (ret_num == -1)                  // User wants to go back
            return -1;

        // Invalid input keep loop going
        print_to_terminal("Invalid option");
        print_to_terminal("Enter the number you wish to select: ");
    }
}

bool CustomerAPI::are_you_sure(const std::string &sure_of_what) {
    terminal_mutex.lock();
    std::cout<< std::endl << "Are you sure "<< sure_of_what << "?" << std::endl;
    std::cout << "1. Yes" << std::endl;
    std::cout << "2. No" << std::endl;
    std::cout << "Your choice: ";
    terminal_mutex.unlock();
    int input = 0;
    while(true)
    {
        input = get_number_input();

        if (input == 1)
            return true;
        if (input == 2)
            return false;

        print_to_terminal("Invalid option");
    }
}


void CustomerAPI::new_pizza()
{
    enum action { BACK = -1, FROM_MENU = 1, NEW_PIZZA };
    std::map<int, Option> new_pizza_options = {
            {1, Option("Select from menu")},
            {2, Option("Start from scratch")}
    };
    std::system("clear");
    int response = terminal_helper(new_pizza_options, "NEW PIZZA MENU");
    switch(action(response))
    {
        case BACK:
            return;
        case NEW_PIZZA:{
            pizzaMaker.new_pizza();
            return;
        }
        case FROM_MENU:{
            while(new_pizza_selection());
            return;
        }
        default:
            return;
    }
}

bool CustomerAPI::new_pizza_selection() {
    std::map<int, Pizza> ref_pizzas = {};
    std::vector<Pizza*> pizzas = pizzaPicker.get_pizzas();
    for (auto pizza: pizzas) {
        ref_pizzas.insert({pizza->id, *pizza});
        std::cout << pizza->name << std::endl;
    }// Generating desired structure for the helper method
    std::system("clear");
    int response = terminal_helper(ref_pizzas, "SELECT PIZZA FROM MENU");
    if (response == -1) return false;

    Pizza selected_pizza = pizzaPicker.select_pizza(response);
    std::system("clear");
    print_to_terminal("This is the pizza: ");
    pizzaPrinter.print(selected_pizza);
    bool wants_selected = are_you_sure("you want this pizza");
    if (wants_selected)
    {
        pizzaMaker.new_pizza(selected_pizza);
        return false;
    }
    return true;
}

bool CustomerAPI::edit_pizza() {
    enum actions { EDIT_SIZE=1, EDIT_BOTTOM, DELETE_TOPPING, ADD_TOPPING, PRINT_PIZZA };
    std::map<int, Option> options = {
            {1, Option("Edit size")},
            {2, Option("Edit bottom")},
            {3, Option("Delete topping")},
            {4, Option("Add topping")},
            {5, Option("Print pizza")}
    };
    if (pizzaMaker.pizzas.empty()){
        print_to_terminal("Currently no pizza selected.");
        press_any_to_continue();
        return false;
    }
    else{
        std::system("clear");
        print_to_terminal("Currently selected pizza: " + pizzaMaker.get_pizza().name);
    }
    int response = terminal_helper(options, "EDIT PIZZA MENU");
    switch (actions(response)) {
        case EDIT_SIZE:{
            while(edit_size());
            return true;
        }
        case EDIT_BOTTOM:{
            while(edit_bottom());
            return true;
        }
        case DELETE_TOPPING:{
            while(delete_topping());
            return true;
        }
        case ADD_TOPPING:{
            while(add_topping());
            return true;
        }
        case PRINT_PIZZA:{
            std::system("clear");
            pizzaPrinter.print(pizzaMaker.get_pizza());
            press_any_to_continue();
            return true;
        }
        // Was -1 which means go back to previous method
        default:
            return false;
    }
}

bool CustomerAPI::edit_size() {
    std::system("clear");
    int response = terminal_helper(pizzaMaker.sizes, "EDIT SIZE");
    if (response == -1 ) return false;
    pizzaMaker.select_size(response);
    return false;
}

bool CustomerAPI::edit_bottom() {
    std::system("clear");
    int response = terminal_helper(pizzaMaker.bottoms, "EDIT BOTTOM");
    if (response == -1) return false;
    pizzaMaker.select_bottom(response);
    return false;
}

bool CustomerAPI::add_topping() {
    std::system("clear");
    int topping_type = terminal_helper(pizzaMaker.toppingTypes, "ADD TOPPING");
    if (topping_type == -1) return false;
    while(add_topping_of_type(topping_type));
    return true;
}

bool CustomerAPI::delete_topping() {
    Pizza curr_pizza = pizzaMaker.get_pizza();
    std::map<int, Option> options = {};

    for (auto* topping: curr_pizza.toppings)
    {
        std::string topping_name = pizzaMaker.toppings[topping->id].name;
        Option option(topping_name);
        options.insert({topping->id, option});
    }
    std::system("clear");
    int response = terminal_helper(options, "DELETE TOPPING");
    if (response == -1) return false;
    int topping_type = pizzaMaker.toppings[response].type;
    pizzaMaker.remove_topping(topping_type, response);
    return true;
}

bool CustomerAPI::add_topping_of_type(int type) {
    std::map<int, Topping> desired_toppings = {};
    for (auto topping: pizzaMaker.toppings)
        if (topping.second.type == type) desired_toppings.insert(topping);
    std::system("clear");
    int topping_id = terminal_helper(desired_toppings, "Add "+pizzaMaker.toppingTypes[type].name+" topping");
    if (topping_id == -1) return false;
    pizzaMaker.select_topping(type, topping_id);
    print_to_terminal("Topping added!");
    press_any_to_continue();
    return true;
}

int CustomerAPI::select_create_helper() {
    if (pizzaMaker.pizzas.empty())
    {
        print_to_terminal("No pizzas have been created!");
        press_any_to_continue();
        return -1;
    }

    std::map<int, Pizza> customer_pizzas = {};
    for (auto pizza: pizzaMaker.pizzas)
        customer_pizzas.insert({pizza.first+1, *pizza.second});

    std::system("clear");
    return terminal_helper(customer_pizzas, "SELECT ONE OF YOUR PIZZAS");
}

bool CustomerAPI::select_created_pizza() {
    int response = select_create_helper();
    if (response == -1) return false;
    response--; // Negating one because we add one to the index in the helper method
    pizzaMaker.select_pizza(response);
    return false;
}

bool CustomerAPI::remove_pizza() {
    int response = select_create_helper();
    if (response == -1) return false;
    response--; // Negating one because we add one to the index in the helper method
    pizzaMaker.remove_pizza(response);
    if (pizzaMaker.pizzas.empty()) return false;
    return true;
}

void CustomerAPI::order_overview() {
    std::system("clear");
    std::lock_guard<std::mutex> guard(terminal_mutex);
    if (deliver && di_complete)
    {
        std::cout << " --- Order for " << deliveryInfo->name << " --- " << std::endl << std::endl;
        std::cout << "DELIVERY INFORMATION" << std::endl;
        std::cout << "Address : " << deliveryInfo->address << std::endl;
        std::cout << "City    : " << deliveryInfo->city << std::endl;
        std::cout << "Postcode: " << deliveryInfo->postal_code << std::endl;
        std::cout << "Phone Nr: " << deliveryInfo->phone << std::endl << std::endl;
    }
    else if (pui_complete)
    {
        std::cout << " --- Order for " << pickUpInfo->name << " --- " << std::endl << std::endl;
        std::cout << "PICKUP INFORMATION" << std::endl;
        std::cout << "Phone Nr: " << pickUpInfo->phone << std::endl << std::endl;
    }
    if (pizzaMaker.pizzas.empty())
    {
        std::cout << "No pizzas yet!" << std::endl;
        return;
    }
    std::cout << "PIZZAS TO ORDER" << std::endl;
    pizzaPrinter.print(pizzaMaker.get_pizzas());

    std::cout << std::endl << "Total price: " << pizzaMaker.get_total_price() << " ISK." << std::endl;
}

bool CustomerAPI::delivery_options() {
    std::map<int, Option> options = {
            {1, Option("Pick Up")},
            {2, Option("Delivery")}
    };
    std::system("clear");
    int response = terminal_helper(options, "DELIVERY OPTIONS");
    if (response == -1) return false;
    if (response == 1)
    {
        deliver=false;
        while(pickup_form());
        return false;
    }
    if (response == 2)
    {
        deliver=true;
        while(delivery_form());
        return false;
    }
    return false;
}

std::string get_input(const std::string& request){
    std::cout << request << ": ";
    while(true)
    {
        std::string input;
        std::getline(std::cin, input);
        if (!input.empty()) return input;
        else std::cout << std::endl << "Invalid input." << std::endl;
    }
}

bool CustomerAPI::pickup_form() {
    pickUpInfo->name  = get_input("Enter full name");
    pickUpInfo->phone = get_input("Enter phone number");
    pui_complete = true;
    return false;
}

bool CustomerAPI::delivery_form() {
    deliveryInfo->name        = get_input("Enter full name");
    deliveryInfo->address     = get_input("Enter address");
    deliveryInfo->city        = get_input("Enter resident city");
    deliveryInfo->postal_code = get_input("Enter postal code");
    deliveryInfo->phone       = get_input("Enter phone number");
    di_complete = true;
    return false;
}

void CustomerAPI::start() {
    while(main());
}

bool CustomerAPI::main() {
    enum actions { QUIT=-1, NEW_PIZZA=1, EDIT_PIZZA, SELECT_CREATED_PIZZA,
                   REMOVE_PIZZA, PRINT_ORDER, PICKUP_DELIVERY, COMPLETE_ORDER };
    std::map<int, Option> options = {
            {1, Option("Add new pizza")},
            {2, Option("Edit current pizza")},
            {3, Option("Select created pizza")},
            {4, Option("Remove created pizza")},
            {5, Option("Print order overview")},
            {6, Option("Pickup & Delivery")},
            {7, Option("Complete order")}
    };
    std::system("clear");
    int response = terminal_helper(options, "MAIN MENU");
    switch(actions(response))
    {
        case QUIT:{
            return false;
        }
        case NEW_PIZZA:{
            new_pizza();
            return true;
        }
        case EDIT_PIZZA: {
            while(edit_pizza());
            return true;
        }
        case SELECT_CREATED_PIZZA:{
            while(select_created_pizza());
            return true;
        }
        case REMOVE_PIZZA:{
            while(remove_pizza());
            return true;
        }
        case PRINT_ORDER:{
            order_overview();
            press_any_to_continue();
            return true;
        }
        case PICKUP_DELIVERY:{
            while(delivery_options());
            return true;
        }
        case COMPLETE_ORDER:{
            if (pizzaMaker.pizzas.empty()){
                print_to_terminal("No pizzas in current order.");
                press_any_to_continue();
            }
            else {
                complete_order();
            }
            return true;
        }
        default:
            return false;
    }
}

void CustomerAPI::server_response_handler(Response *response) {
    if (response->getStatus() == SUCCESS)
    {
        switch(response->getType())
        {
            case RES_MESSAGE:{
                order_complete = true;
                pizzaMaker.clear();
                std::string message_from_server = Parser::parse_message(response);
                print_to_terminal("Message from server: "+message_from_server);
                return;
            }
            case RES_BAKER_STATUS:{
                std::string message_from_server = Parser::parse_message(response);
                print_to_terminal("Pizza status update from baker: " + message_from_server);
                return;
            }
            default:
                print_to_terminal("Received an unrecognized response from server");
                return;
        }
    }
    if (response->getStatus() == ERROR)
    {
        switch(response->getType())
        {
            case RES_MESSAGE:{
                std::string message_from_server = Parser::parse_message(response);
                print_to_terminal("Error message from server: "+message_from_server);
                return;
            }
            default:
                print_to_terminal("Received an un-parsable error message from server");
                return;
        }
    }
    if (response->getStatus() == HANDSHAKE_SUCCESS)
    {
        std::string message_from_server = Parser::parse_message(response);
        print_to_terminal("Message from server: "+message_from_server);
        return;
    }
    print_to_terminal("Received an unrecognized status type from server");
}

void CustomerAPI::complete_order() {
    if (client == nullptr)
    {
        print_to_terminal("No client is connected to the customer API");
        press_any_to_continue();
        return;
    }
    if (!client->connected)
    {
        print_to_terminal("The client socket is not connected to the server");
        press_any_to_continue();
        return;
    }
    if (deliver)
    {
        if (!di_complete)
        {
            print_to_terminal("Delivery information incomplete. Fill out the form and try again.");
            press_any_to_continue();
            return;
        }
        print_to_terminal("Please wait while the server processes your order.");
        int total_price = pizzaMaker.get_total_price();
        PizzaOrder pizza_order = PizzaOrder(pizzaMaker.get_pizzas(), deliveryInfo, total_price);
        json json_pizza_order;
        PizzaOrder::to_json(pizza_order, json_pizza_order);
        Request request = Request(REQ_ORDER, "Customer", json_pizza_order);
        client->sendToServer(request);
        return;
    }
    else {
        if (!pui_complete)
        {
            print_to_terminal("Pick-up information incomplete. Fill out the form and try again.");
            press_any_to_continue();
            return;
        }
        print_to_terminal("Please wait while the server processes your order.");
        int total_price = pizzaMaker.get_total_price();
        PizzaOrder pizza_order = PizzaOrder(pizzaMaker.get_pizzas(), pickUpInfo, total_price);
        json json_pizza_order;
        PizzaOrder::to_json(pizza_order, json_pizza_order);
        Request request = Request(REQ_ORDER, "Customer", json_pizza_order);
        client->sendToServer(request);
        return;
    }
}

void CustomerAPI::add_client(Client *client_socket) {
    client = client_socket;
}

void CustomerAPI::print_to_terminal(const std::string &message) {
    terminal_mutex.lock();
    std::cout << message << std::endl;
    terminal_mutex.unlock();
}


CustomerAPI::CustomerAPI() = default;

