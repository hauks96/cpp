#include "BakeryAPI/BakeryAPI.h"
#include <functional>
#include <thread>
#include <chrono>
using namespace std::placeholders;

int main() {
    BakeryAPI bakery;
    auto function_pointer = std::bind(&BakeryAPI::server_response_handler, &bakery, _1); //_1 means take one argument
    auto* client = new Client(PIZZA_BAKER, function_pointer);
    client->start();
    std::this_thread::sleep_for (std::chrono::seconds(1));
    bakery.add_client(client);
    bakery.start();
    return 0;
}
