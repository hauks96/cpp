//
// Created by agirm on 23/02/2021.
//
#include "HuffmanTree.h"
#include <iostream>
#include <bitset>
#include <utility>
#define BIT_SIZE 8


/*
 * ENCODING IMPLEMENTATION
 *
 * Helper method of byteToCodeMap()
 *
 * Recursively generates a hashmap that can be used to look up the bitcode of a given letter
 * Return map: { byte/letter: {bitcode, bitcode length}}
 * Unordered map has average O(1) lookup time
 * */
void recursiveMap(Node *node, std::unordered_map<char, std::vector<unsigned char>>& byteMap, std::vector<unsigned char> &bitcode, char bit, unsigned int acc, int bitsLeft) {
    if (node == nullptr){
        return;
    }
    // Create a new char byte
    if (bitsLeft == 0){
        bitcode.push_back(0);
        bitsLeft = 8;
    }
    // Add new bit to current bitcode (0 or 1)
    int index = (int) bitcode.size()-1;
    bitcode[index] = bitcode[index] << 1;
    bitcode[index] = bitcode[index] | bit;

    // When we're at a leaf we push the current code into the byteCodeMap
    if (node->left == nullptr && node->right == nullptr){
        // copy for insert and push acc to back
        std::vector<unsigned char> copy = bitcode;
        unsigned int shift = (8-acc%8);

        // Remove any bits that are not supposed to be in the bitcode (Anything above accumulated bits)
        if (shift!=8){
            copy[index] = (copy[index] << shift);
            copy[index] = copy[index] >> shift;
        }

        // Add the last char byte in vector as size of bitcode
        copy.push_back((unsigned char) acc);
        byteMap.insert(std::make_pair(node->byte, std::move(copy)));
        return;
    }
    // Necessary copy for implementation to work recursively
    std::vector<unsigned char> copy = bitcode;
    recursiveMap(node->left, byteMap, copy, 1, acc+1, bitsLeft-1);
    copy = bitcode;
    recursiveMap(node->right, byteMap, copy, 0, acc+1, bitsLeft-1);
}

/*
 * ENCODING IMPLEMENTATION
 *
 * Generates a byte to code map given the current huffman tree.
 * Helper method of the encoding constructor.
 *
 */
std::unordered_map<char, std::vector<unsigned char>> byteToCodeMap(Node* root)  {
    std::unordered_map<char, std::vector<unsigned char>> bitcodeHashMap;
    if (root == nullptr){
        throw std::runtime_error("Trying to create byte to code map from empty tree.");
    }
    std::vector<unsigned char> initial_bitcode = std::vector<unsigned char>{0};
    recursiveMap(root->left, bitcodeHashMap, initial_bitcode, 1, 1, 8);
    initial_bitcode = std::vector<unsigned char>{0};
    recursiveMap(root->right, bitcodeHashMap, initial_bitcode, 0, 1, 8);


    /* Some beautiful printing of the bit-codes
     * Can be ignored.
     *
    for (auto const& pair: bitcodeHashMap){
        std::cout << pair.first << ": ";
        std::vector<unsigned char> the_bitcode = pair.second;
        auto code_size = (unsigned int) the_bitcode.back();
        unsigned char shifts = 0;
        if (code_size>8){
            shifts = 8-code_size%8;
            if (shifts == 8){
                shifts = 0;
            }
        }
        the_bitcode.pop_back();
        for (int i=0; i<the_bitcode.size(); i++){
            if (i == the_bitcode.size()-1){
                unsigned char byte = the_bitcode[i];
                byte = byte << shifts;
                std::bitset<BIT_SIZE> bit(byte);
                std::cout << bit;
            }
            else {
                if (the_bitcode[i] == ' '){
                    std::cout << "ITS THE SPACE ONE!!!" << std::endl;
                }
                std::bitset<BIT_SIZE> bit(the_bitcode[i]);
                std::cout << bit << "-";
            }
        }
        std::cout << ": Bitcode size: " << code_size;
        std::cout << std::endl;
    }*/
    return bitcodeHashMap;
}

/*
 * ENCODING IMPLEMENTATION
 *
 * Allows retrieving the generated bitcode map after construction of the huffman tree.
 */
std::unordered_map<char, std::vector<unsigned char>> HuffmanTree::getByteToCodeMap() const {
    if (this->encodeInit){
        return this->bitmap;
    }
    throw std::runtime_error("Initialized HuffmanTree for decoding. Cannot retrieve ByteToCodeMap");
}

/*
 * ENCODING IMPLEMENTATION
 *
 * Helper method of encoding constructor.
 * Generates a new subtree for the current tree in the desired format.
 */
void newSubtree(HuffmanTree &ht, Node *leftLeaf, Node *rightLeaf){
    Node* subtree = new Node(0, leftLeaf->sum+rightLeaf->sum);
    subtree->left = leftLeaf;
    subtree->right = rightLeaf;

    ht.root->right = subtree;
    ht.root->sum +=subtree->sum;
    Node* new_root = new Node(0, ht.root->sum);
    new_root->left = ht.root;
    ht.root = new_root;
}

/*
 * ENCODING IMPLEMENTATION
 *
 * Helper method of encoding constructor.
 * Generates a new leaf for the current tree in the desired format.
 */
void newLeaf(HuffmanTree& ht, Node* leaf){
    if (ht.root->left == nullptr){
        ht.root->left = leaf;
        ht.root->sum += leaf->sum;
    }
    else if (ht.root->right == nullptr){
        ht.root->right = leaf;
        ht.root->sum += leaf->sum;
        Node* new_root = new Node(0, ht.root->sum);
        new_root->left = ht.root;
        ht.root = new_root;
    }
    else {
        std::cout << "Neither!" << std::endl;
    }
}

void endLeaf(HuffmanTree& ht, Node* leaf){
    ht.root->right = leaf;
    ht.root->sum +=leaf->sum;
}

/*
 * ENCODING IMPLEMENTATION
 *
 * Constructor for encoding methods.
 *
 * Iteratively generates a huffman tree given a frequency map of the characters in the file.
 * FrequencyVector: {Byte: Frequency}
 * */
HuffmanTree::HuffmanTree(std::vector<std::pair<char, int>> freqVector){
    this->headerSize = 0;
    this->encodeInit = true;
    this->decodeInit = false;

    this->root = new Node(0, 0);

    std::pair<char, int> current_pair = freqVector.back();
    char byte = current_pair.first;
    int frequency = current_pair.second;
    Node* leaf = new Node(byte, frequency);
    freqVector.pop_back();

    while(!freqVector.empty()){
        current_pair = freqVector.back();
        byte = current_pair.first;
        frequency = current_pair.second;

        if (leaf->sum + this->root->sum < leaf->sum + frequency || freqVector.size() == 1){
            // if it's the last one we don't want to create a new root
            newLeaf(*this, leaf);
            leaf = new Node(byte, frequency);
            freqVector.pop_back();
            if (freqVector.empty()){
                endLeaf(*this, leaf);
            }
        }
        else {
            Node* right_node = new Node(byte, frequency);
            newSubtree(*this, leaf, right_node);
            freqVector.pop_back();
            leaf = new Node(freqVector.back().first, freqVector.back().second);
            freqVector.pop_back();
            if (freqVector.empty()){
                endLeaf(*this, leaf);
            }
        }
    }
    this->bitmap = byteToCodeMap(this->root);
}


/*
 * ENCODING HUFFMAN IMPLEMENTATION
 *
 * Inserts a byte into a given position of an array. The leftover bits that don't
 * fit will shift into the next byte of the array
 *
 * acc: Current byte being worked on in the string
 * shift: The position from start of byte at which the insertion should start
 * byte: The byte to insert
 * */
void insertByte(unsigned char* string, int* acc, int* shift, unsigned char byte){
    if (shift[0]<0){
        shift[0]=7;
        acc[0]++;

        string[acc[0]]=byte;
        acc[0]++;
    }
    else if (shift[0]==7){
        string[acc[0]]=byte;
        acc[0]++;
    }
    else {
        int downShift = BIT_SIZE-(shift[0]+1);
        int upShift = BIT_SIZE-downShift;
        string[acc[0]] = string[acc[0]] | byte>>downShift;
        acc[0]++;
        string[acc[0]] = string[acc[0]] | byte << (upShift);
    }
}


/*
 * ENCODING IMPLEMENTATION
 *
 * Helper method of createHeader
 * Using preorder traversal, recursively create a header for a huffman tree.
 * Every 1 is path and 0 is leaf in the header. Immediately after a 0 comes a bytecode 8 bits long
 *
 * string: The generated header
 * acc: At what byte we are at in the header construction
 * shift: The position from start of byte at which the insertion should start
 */
void headGenRec(Node* node, unsigned char* string, int* acc, int* shift){
    if (shift[0]==-1){
        shift[0]=7;
        acc[0]++;
    }
    if (node == nullptr){
        return; // should never happen
    }

    if (node->left == nullptr && node->right == nullptr){
        shift[0]--;
        insertByte(string, acc, shift, node->byte);
        return;
    }

    else {
        string[acc[0]] = string[acc[0]] | 1<<shift[0];
        shift[0]--;
    }

    headGenRec(node->left, string, acc, shift);
    headGenRec(node->right, string, acc, shift);
}

/*
 * ENCODING IMPLEMENTATION
 *
 * Generate a header given the constructed huffman tree.
 */
char *HuffmanTree::createHeader() {
    if (this->root== nullptr){
        std::cout << "No items in tree" << std::endl;
        return nullptr;
    }
    auto* tempHeader = new unsigned char[5000];
    for (int i=0; i<5000; i++){
        tempHeader[i]=0;
    }
    int acc[1] = {0};
    int shift[1] = {7};
    headGenRec(this->root, tempHeader, acc, shift);

    int header_size = acc[0]+1;

    /* Add bit count to header
     * We know that there is a maximum of 256 types of bytes in the tree
     * and the amount of branches in the tree can never exceed 2x256.
     * That means the maximum number of bits in the tree is: 256*8 + 256 + 2x256 = 2816
     * We need at most a short (2 bytes) to represent the bit count. */
    unsigned short bits = (acc[0]+1)*8-(shift[0]+1);
    unsigned char bit_count[2] = {0};
    bit_count[0]= (bits & 0xFF00) >> 8;
    bit_count[1]= bits & 0xFF;

    // Generate final header with bit count as first two bytes
    header_size+=2; // +2 For the bit count
    auto* header = new char[header_size];
    header[0]=bit_count[0];
    header[1]=bit_count[1];
    for (int i=2; i<header_size; i++){
        header[i]=tempHeader[i-2];
    }
    delete[] tempHeader;

    /* Printing header
    std::cout << "Header (w size byte): ";
    for (int i=0; i<header_size; i++){
        std::bitset<8> bit(header[i]);
        std::cout << bit << "-";
    }
    std::cout << std::endl;*/

    this->headerSize = header_size;
    return header;
}

/*
 * ENCODING IMPLEMENTATION
 *
 * To retrieve the size of the header
 * Only use with encoding initialization.
 */
int HuffmanTree::getHeaderSize() const {
    if (this->decodeInit){
        throw std::runtime_error("Huffman tree initialized as decoder! Cannot get header size.");
    }
    return this->headerSize;
}


/*
 * DECODE HUFFMAN IMPLEMENTATION
 *
 * Generates a huffman tree from an encoded file header
 */
void decodeHead(BinaryArray& binaryArray, Node* node){
    unsigned char bit;
    unsigned char data;
    while(true){
        if (node->left != nullptr && node->right != nullptr){
            return;
        }
        bit = binaryArray.get_next_bit();

        if (bit == 1){
            if (node->left == nullptr){
                node->left = new Node(0, 0);
                decodeHead(binaryArray, node->left);
            } else if (node->right == nullptr) {
                node->right = new Node(0, 0);
                decodeHead(binaryArray, node->right);
            }
        } else if (bit == 0){
            if (node->left == nullptr) {
                data = binaryArray.get_byte();
                node->left = new Node((char) data, 0);
            } else if (node->right == nullptr) {
                data = binaryArray.get_byte();
                node->right = new Node((char) data, 0);
            }
        }
    }
}


/*
 * DECODE HUFFMAN IMPLEMENTATION
 *
 * Constructor for decoding of a compressed file. Takes the file header and
 * it's bit count as arguments and automatically generates the huffman tree.
 */
HuffmanTree::HuffmanTree(std::vector<char> fileHeader, int maxBits) {
    this->headerSize = -1;
    this->decodeInit = true;
    this->encodeInit = false;
    this->decodeExtraBits = std::vector<char>(0);
    this->alreadyTaken = 0;
    this-> remaining = 0;
    this->root = new Node(0,0);
    BinaryArray binaryArray = BinaryArray(std::move(fileHeader), 0);
    binaryArray.remainingBits = maxBits;
    binaryArray.get_next_bit();
    decodeHead(binaryArray, this->root);
}

/*
 * DECODING IMPLEMENTATION
 *
 * Helper method of decode()
 *
 * Recursively go left and right in the tree until a node is found.
 * Return the letter in the given node.
 *
 * When bit is 1: Go down left
 * When bit is 0: Go down right
 * Note: This method would be better suited as part of the Decoder class
 */
unsigned char find_char(BinaryArray& binaryArray, Node* node, int& readSinceLastReturn, bool& incompleteBitCode){
    if (node == nullptr){
        throw std::runtime_error("HuffmanTree::find_char() - IN NULL NODE! ");
    }
    if (node->left == nullptr && node->right == nullptr){
        return node->byte;
    }
    if (node->left == nullptr && node->right != nullptr || node->left != nullptr && node->right == nullptr){
        throw std::runtime_error("ERROR one node is null!!!");
    }
    if (binaryArray.remainingBits == 0){
        incompleteBitCode=true;
        return 0;
    }
    unsigned char bit = binaryArray.get_next_bit();
    if (bit==1){
        readSinceLastReturn += 1;
        return find_char(binaryArray, node->left,readSinceLastReturn, incompleteBitCode);
    }
    else {
        readSinceLastReturn += 1;
        return find_char(binaryArray, node->right,readSinceLastReturn, incompleteBitCode);
    }
}

/*
 * DECODING IMPLEMENTATION
 *
 * Decodes a stream from an encoded file.
 * isLast: Notify the method that the stream as the last one in the file.
 * Note: This method would be better suited as part of the Decoder class
 */
std::vector<char> HuffmanTree::decode(std::vector<char> stream, bool isLast) {
    std::vector<char> fileContent;

    /*
     * A reference boolean is passed into recursive character decode method which notifies us if we didn't
     * manage to finish the last bitcode with the stream given to us
     */
    bool nextIsIncomplete = false;
    /*
     * readSinceLastReturn is passed into the recursive character decoding method and tells us
     * how many bits were read that didn't decode into a letter. We need to store this for the next stream we receive
     * */
    int readSinceLastReturn = 0;

    /*
     * isLast tells us whether we are currently working on the last chunk of data for decoding.
     * This is important because the last byte in the encoded file tells us how many bits we need to read
     * in the second last byte before stopping the decode process.
     * */
    if (isLast){
        // Extract the last byte
        auto bitsToRead = (unsigned char) stream.back();
        stream.pop_back();
        /* We initialize the binary array like normally but we deduct the bits
         * that we shouldn't read from the 'remaining bits' variable
         * */
        BinaryArray binaryArray(std::move(stream), 0);
        binaryArray.remainingBits-= (unsigned int) (8-bitsToRead);
        /*
         * If there are remaining bits that didn't complete a bitcode in the previous iteration
         * we need to add them to the binary array so that they are read before the new stream.
         * */
        if (this->remaining != 0){
            // Add to binary array
            binaryArray.extraRemainingBits = this->remaining;
            binaryArray.extraAlreadyTaken = this->alreadyTaken;
            binaryArray.extra = std::move(this->decodeExtraBits);
            // Reset
            this->remaining = 0;
            this->alreadyTaken = 0;
            this->decodeExtraBits = std::vector<char>(0);
        }
        /* Here we recursively iterate the tree to get the characters from the bit-codes
         * until there are no remaining bits in the current stream */
        while(binaryArray.remainingBits>0){
            unsigned char byte = find_char(binaryArray, this->root, readSinceLastReturn, nextIsIncomplete);
            readSinceLastReturn = 0;
            fileContent.emplace_back((char) byte);
        }

    }
    else {
        // Initialize the binary array with the given stream
        BinaryArray binaryArray(std::move(stream), 0);


        /*
         * If there are remaining bits that didn't complete a bitcode in the previous iteration
         * we need to add them to the binary array so that they are read before the new stream.
         * */
        if (this->remaining != 0){
            binaryArray.extraRemainingBits = this->remaining;
            binaryArray.extraAlreadyTaken = this->alreadyTaken;
            binaryArray.extra = std::move(this->decodeExtraBits);
            // Reset
            this->remaining = 0;
            this->alreadyTaken = 0;
            this->decodeExtraBits = std::vector<char>(0);
        }

        /* Here we recursively iterate the tree to get the characters from the bit-codes
         * until there are no remaining bits in the current stream
         * */
        while(binaryArray.remainingBits>0){
            char byte = (char) find_char(binaryArray, this->root, readSinceLastReturn, nextIsIncomplete);
            /* If the last bitcode was not incomplete
             * i.e the recursion returned a character on the last bit in the stream we don't have to store anything
             * for next iteration */
            if (!nextIsIncomplete){
                fileContent.emplace_back(byte);
                readSinceLastReturn = 0;
            }
            else {
                /*
                 * The last bit did not return a decoded character so we need to store the data
                 * so that it can be used in the next iteration
                 * */
                this->remaining = (int) binaryArray.remainingBits;
                this->alreadyTaken = (int) binaryArray.alreadyTaken%8;

                /*
                 * We need BytesToReRead in order to know how many bytes at the back of the current stream
                 * we have to store for the next iteration.
                 */
                int bytesToReRead = (readSinceLastReturn/8);
                // If it was rounded down by int division add 1
                if (readSinceLastReturn%8 != 0){
                    bytesToReRead++;
                }
                // Store the bytes in the correct order in a vector for next iteration
                int binaryArraySize = binaryArray.myArray.size();
                for (int i=bytesToReRead; i>0; i--){
                    this->decodeExtraBits.emplace_back(binaryArray.myArray[binaryArraySize-bytesToReRead]);
                }
            }

        }
    }
    return fileContent;
}
