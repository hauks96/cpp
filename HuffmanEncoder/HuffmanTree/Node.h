//
// Created by agirm on 23/02/2021.
//

#ifndef HUFFMANENCODER_NODE_H
#define HUFFMANENCODER_NODE_H

struct Node {
    Node(char byte, int sum){
        this->byte = byte;
        this->sum = sum;
        this->left = nullptr;
        this->right = nullptr;
    }
    ~Node(){
        delete this->left;
        delete this->right;
    }
    Node* left;
    Node* right;
    unsigned char byte;
    int sum;
};


#endif //HUFFMANENCODER_NODE_H
