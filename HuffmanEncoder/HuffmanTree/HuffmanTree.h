//
// Created by agirm on 23/02/2021.
//

#ifndef HUFFMANENCODER_HUFFMANTREE_H
#define HUFFMANENCODER_HUFFMANTREE_H

#include <vector>
#include <unordered_map>
#include "Node.h"
#include "../BinaryArray/BinaryArray.h"

class HuffmanTree {
public:
    ~HuffmanTree(){
        delete root;
    }

    // Constructor of tree for decoding
    explicit HuffmanTree(std::vector<char> fileHeader, int maxBits);

    // Decode a char vector and return a new one (Decoding initialization required)
    std::vector<char> decode(std::vector<char> stream, bool isLast);

    // Constructor of tree for encoding
    explicit HuffmanTree(std::vector<std::pair<char, int>> freqVector);

    // Map to get bit code from bytes
    std::unordered_map<char, std::vector<unsigned char>> getByteToCodeMap() const;

    // Create file header
    char* createHeader();

    // Get file header size
    int getHeaderSize() const;
    Node* root{};

private:
    std::unordered_map<char, std::vector<unsigned char>> bitmap;
    int headerSize;
    std::vector<char> decodeExtraBits; // Remaining bits from previous iterations of decoding
    int alreadyTaken;
    int remaining;
    // Warn user of incorrect getter methods
    bool encodeInit;
    bool decodeInit;
};

#endif //HUFFMANENCODER_HUFFMANTREE_H
