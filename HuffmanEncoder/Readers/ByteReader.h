//
// Created by agirm on 25/02/2021.
//

#ifndef HUFFMANENCODER_BYTEREADER_H
#define HUFFMANENCODER_BYTEREADER_H
#include <vector>
#include <iostream>
#include <fstream>
#include <limits>

class ByteReader{
public:
    ByteReader();
    /* Initializes a byte reader */
    void initByteReader(const char* filename);

    /* Get the stream of size of buffer as a vector */
    std::vector<char> getVectorBufferStream();

    /* Get the vector stream of user defined size in bytes */
    std::vector<char> getVectorBufferStream(int byteAmount);

    /* Get the stream of size of buffer as a char array.
     * Must be deallocated by user. */
    char* getArrayBufferStream();

    /* Get the array stream of user defined size in bytes */
    const char* getArrayBufferStream(int byteAmount);

    /* For encoding only.
     * Returns total frequency of bytes in file in sorted order from least significant to largest.
     * Reads entire initialized file on call */
    std::vector<std::pair<char, int>> getByteFrequencyMap();

    /* Returns the total size of file in bytes */
    int getFileSize() const;

    /* Returns the size of the last retrieved buffer */
    int getLatestBufferSize() const;

    /* Returns true if file has been read completely */
    bool eof() const;

    /* Returns the last character in file. Does not alter read count */
    char lastByte();

    int remainingBytes() const;

    void reduceReadCount(int howMany);

    /* Reset read count and remaining lines to the values acquired at initialization*/
    void resetReadCount();

private:
    int fileByteSize;
    int fileReadBytes;
    int fileRemainingBytes;
    const char* filename;
    bool initByte;
    int latestBufferSize;

    void setInitial(){
        this->fileByteSize = 0;
        this->fileReadBytes = 0;
        this->initByte = false;
        this->filename = nullptr;
        this->latestBufferSize = 0;
    }
    void initialRead(const char* infile){
        // Open file in binary mode
        std::ifstream f_in(infile, std::ios::binary);
        if (!f_in.is_open()){
            throw std::runtime_error("File not found.");
        }

        // Get total byte size of file
        f_in.ignore( std::numeric_limits<std::streamsize>::max() );
        std::streamsize fileSize = f_in.gcount();

        if (fileSize<0){
            throw std::runtime_error("Error reading file.");
        }

        f_in.clear();
        f_in.close();
        this->fileRemainingBytes = fileSize;
        this->fileByteSize = fileSize;
    }

};
#endif //HUFFMANENCODER_BYTEREADER_H
