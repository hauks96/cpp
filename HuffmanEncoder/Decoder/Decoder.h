//
// Created by agirm on 24/02/2021.
//

#ifndef HUFFMANENCODER_DECODER_H
#define HUFFMANENCODER_DECODER_H

#include <vector>
#include "../HuffmanTree/HuffmanTree.h"
#include "../Readers/ByteReader.h"

class Decoder {
    friend void decompressFile(Decoder& dc, const std::string& output_filename);

public:
    void decode(const std::string& input_filename, const std::string& output_filename);

    // ByteReader for streaming file data
    ByteReader* byteReader{};

    // Huffman tree for decoding
    HuffmanTree* huffmanTree{};
};

#endif //HUFFMANENCODER_DECODER_H
