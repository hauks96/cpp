//
// Created by agirm on 23/02/2021.
//
#define WRITE_SIZE 100000 // 100k Bytes ~ 100kiB
#define BYTE_SIZE 8
#include "Encoder.h"
#include <fstream>
#include <iostream>
#include "../BinaryArray/BinaryArray.h"


void compressFile(Encoder& ec, const std::string& output_filename){
    std::ofstream out;
    out.open(output_filename, std::ios::binary);

    /* Generate a file header by using the huffman tree */
    char* header = ec.huffmanTree->createHeader();
    int headerSize = ec.huffmanTree->getHeaderSize();
    out.write(header, headerSize);

    /* We use a byte-to-code map to get the code for each character in the file */
    std::unordered_map<char, std::vector<unsigned char>> bitmap = ec.huffmanTree->getByteToCodeMap();

    std::vector<unsigned char> write_buffer(WRITE_SIZE);
    for (int i=0; i<WRITE_SIZE; i++){
        write_buffer[i]=0;
    }
    BinaryArray binaryArray(write_buffer, WRITE_SIZE, true);

    // Streaming in from file to read with a fixed buffer size
    while(!ec.byteReader->eof()){

        std::vector<char> stream = ec.byteReader->getVectorBufferStream();
        int stream_size = stream.size();

        for (int i=0; i<stream_size; i++){
            auto byteToEncode = (unsigned char) stream[i];
            std::vector<unsigned char> current_bitcode = bitmap[byteToEncode];
            int bitCodeBitLength = current_bitcode.back();
            unsigned int bitCodeByteSize = current_bitcode.size()-1;

            for (int j=0; j<bitCodeByteSize; j++){
                unsigned char bitcode_byte = current_bitcode[j];
                unsigned int length = 8;
                if (j==bitCodeByteSize-1){
                    length = 8-(8-(bitCodeBitLength%8));
                    if (length == 0){
                        length = 8;
                    }
                    else {
                        bitcode_byte = bitcode_byte<<(8-length);
                    }
                }

                binaryArray.set_bits(length, bitcode_byte);

                if (binaryArray.remainingBits == 0){
                    out.write((char*)(&binaryArray.setArray[0]), WRITE_SIZE);
                    binaryArray.alreadyTaken = 0;
                    binaryArray.remainingBits = WRITE_SIZE*8;
                    for (int k=0; k<WRITE_SIZE; k++){
                        binaryArray.setArray[k]=0;
                    }
                }
            }
        }
    }

    // Last byte in file says how many bits are used in the one before the last
    if (binaryArray.remainingBits>0){
        unsigned int bytesToWrite = binaryArray.alreadyTaken/8;
        if (binaryArray.alreadyTaken%8 != 0){
            bytesToWrite++;
        }
        out.write((char*)(&binaryArray.setArray[0]), bytesToWrite);
    }
    char end_info[1];
    end_info[0]=BYTE_SIZE-(binaryArray.remainingBits%8);
    out.write(end_info, 1);
    out.close();
}

void Encoder::encode(const std::string&  input_filename, const std::string& output_filename) {
    this->byteReader = new ByteReader();
    this->byteReader->initByteReader(input_filename.c_str());
    std::vector<std::pair<char, int>> byteFrequency = this->byteReader->getByteFrequencyMap();
    this->huffmanTree = new HuffmanTree(byteFrequency);
    compressFile(*this, output_filename);
}




