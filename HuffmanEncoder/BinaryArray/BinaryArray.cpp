#include "BinaryArray.h"

unsigned char BinaryArray::get_next_bit(){
    if (this->extraRemainingBits != 0){
        return this->get_extraBit();
    }
    unsigned int current_index = alreadyTaken/8;
    unsigned int current_bit_position = 8-(alreadyTaken%8); // Already Taken: XXXY-YYYY x-taken = 3  bitpos = 5
    unsigned int neededDownShift = current_bit_position-1;
    auto byteToMask = (unsigned char) myArray[current_index];
    unsigned char return_bit = (byteToMask >> neededDownShift) & 0x1;
    alreadyTaken++;
    remainingBits--;
    return return_bit;
}

unsigned char BinaryArray::get_byte(){
    unsigned int current_index = alreadyTaken/8;
    unsigned int current_bit_position = 8-(alreadyTaken%8); // bit position from left XXYY-YYYY
    if (current_bit_position == 8){
        alreadyTaken+=8;
        remainingBits-=8;
        return (unsigned char) myArray[current_index];
    }
    else {
        unsigned int neededUpShift = 8-current_bit_position;
        auto charToMask = (unsigned char) myArray[current_index];
        unsigned char firstHalf = charToMask << neededUpShift;

        // YYYY-YYYY -> take upper 2 -> XXYY-YYYY
        current_index++;
        unsigned int neededDownShift = 8-neededUpShift;
        charToMask = myArray[current_index];
        unsigned char secondHalf = charToMask >> neededDownShift;

        alreadyTaken+=8;
        remainingBits-=8;
        unsigned char toReturn = firstHalf | secondHalf;
        return toReturn;
    }
}


unsigned char BinaryArray::get_extraBit() {
    unsigned int current_index = extraAlreadyTaken/8;
    unsigned int current_bit_position = 8-(extraAlreadyTaken%8); // Already Taken: XXXY-YYYY x-taken = 3  bitpos = 5
    unsigned int neededDownShift = current_bit_position-1;
    auto byteToMask = (unsigned char) extra[current_index];
    unsigned char return_bit = (byteToMask >> neededDownShift) & 0x1;
    extraAlreadyTaken++;
    extraRemainingBits--;
    return return_bit;
}


void BinaryArray::set_bits(unsigned int numberOfBits, unsigned char byteToSet) {
    if (extraRemainingBits != 0){
        unsigned int numOfBits = extraRemainingBits;
        unsigned char bToSet = extraByte;
        extraRemainingBits = 0;
        extraByte = 0;
        set_bits(numOfBits, bToSet);
    }
    unsigned int index = alreadyTaken/8;

    unsigned int possibleTake;
    if (alreadyTaken%8 == 0){
        possibleTake = 8;
    }
    else {
        possibleTake = 8-alreadyTaken%8;
    }
    unsigned int toTake;

    if (possibleTake<numberOfBits){
        if (alreadyTaken%8 == 0){
            toTake = 8;
        }
        else {
            toTake = possibleTake; // XXXY-YYYY = 5
        }
    }else {
        toTake = numberOfBits;
    }

    if (numberOfBits>remainingBits){
        extraRemainingBits = numberOfBits-remainingBits; // XXXX-XYYY (Y remaining)
        unsigned char byteToStore = byteToSet << toTake;
        this->extraByte = byteToStore;
        toTake = remainingBits;
    }


    unsigned char byte_copy = byteToSet >> (8-toTake); // AAAA-AAAA -> 000A-AAAA
    this->setArray[index] = this->setArray[index] | (byte_copy << (possibleTake-toTake));  // XXXA-AAAA

    if (toTake<numberOfBits && remainingBits-toTake!=0){
        alreadyTaken+= toTake;
        remainingBits-= toTake;
        unsigned char new_byte = byteToSet << toTake; // AAAA-AAAA -> AAA0-0000 (To take the remaining bits)
        set_bits(numberOfBits-toTake, new_byte);
    }
    else {
        alreadyTaken+= toTake;
        remainingBits-= toTake;
    }
}


