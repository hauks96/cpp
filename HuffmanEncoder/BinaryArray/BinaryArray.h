//
// Created by Hakon Hakonarson on 24.2.2021.
//
#ifndef HUFFMANENCODER_BINARYARRAY_H
#define HUFFMANENCODER_BINARYARRAY_H
#include <utility>
#include <vector>
class BinaryArray {
public:
    std::vector<char> myArray;
    std::vector<char> extra;
    unsigned extraAlreadyTaken;
    unsigned alreadyTaken;
    unsigned int remainingBits;
    unsigned int extraRemainingBits;
    unsigned char get_extraBit();
    unsigned char get_next_bit();
    unsigned char get_byte();
    explicit BinaryArray(std::vector<char> arr, int bitsFromStart){
        this->remainingBits = arr.size()*8;
        this->myArray = std::move(arr);
        this->alreadyTaken = bitsFromStart;
        this->extraAlreadyTaken = 0;
        this->extraRemainingBits = 0;
    }
    explicit BinaryArray(std::vector<unsigned char> arr, int arraySize, bool hello){
        this->extraRemainingBits = 0;
        this->extraAlreadyTaken = 0;
        this->setArray = std::move(arr);
        this->alreadyTaken = 0;
        this->remainingBits = arraySize*8;
    }
    std::vector<unsigned char> setArray;
    unsigned char extraByte;
    void set_bits(unsigned int numberOfBits, unsigned char byte);


};

#endif //HUFFMANENCODER_BINARYARRAY_H
